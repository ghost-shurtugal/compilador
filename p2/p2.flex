%{
	#include <stdio.h>
%}

id [a-zA-Z]+
espacio [ \t\n}]+

%%

.	{printf("Ha ocurrido un error léxico %s\n",yytext);}
{id}+	 {printf("Encontré un identificador %s\n",yytext);}
int	{printf("Encontré una palabra reservada %s\n",yytext);}
float	 {printf("Encontré una palabra reservada %s\n",yytext);}
if	 {printf("Encontré una palabra reservada %s\n",yytext);}
else	{printf("Encontré una palabra reservada %s\n",yytext);}
{espacio}	 {/*Ignorar los espacios en blanco*/}


%%

int main(){
	yylex();
}
