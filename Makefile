version.o:   .FORCE

.FORCE:
	cd build;bison -d -v  ../src/gramatica.y;flex ../src/lexer.l;gcc ../src/gramatica_utils.c ../src/tabs.c gramatica.tab.c lex.yy.c  -lfl -o ../proyecto

test1:
	./proyecto entradas/prueba1.txt salidas/salida1.txt

test2:
	./proyecto entradas/prueba2.txt salidas/salida2.txt

test3:
	./proyecto entradas/prueba3.txt salidas/salida3.txt

testAll:
	./proyecto entradas/prueba1.txt salidas/salida1.txt
	./proyecto entradas/prueba2.txt salidas/salida2.txt
	./proyecto entradas/prueba3.txt salidas/salida3.txt
