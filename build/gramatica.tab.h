/* A Bison parser, made by GNU Bison 3.0.5.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_GRAMATICA_TAB_H_INCLUDED
# define YY_YY_GRAMATICA_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    TOKEN_ENTERO = 258,
    TOKEN_FLOTANTE = 259,
    TOKEN_CADENA = 260,
    TOKEN_ID = 261,
    TOKEN_CARACTER = 262,
    TOKEN_DOBLE_PRECISION = 263,
    TOKEN_VACIO = 264,
    TOKEN_PALABRA_RESERVADA_FALSO = 265,
    TOKEN_PALABRA_RESERVADA_CIERTO = 266,
    TOKEN_OPERADOR_RELACIONAL = 267,
    TOKEN_TIPO = 268,
    TOKEN_TIPO_REGISTRO = 269,
    TOKEN_TIPO_FUNCION = 270,
    TOKEN_TIPO_PARAM = 271,
    TOKEN_ERROR = 272,
    TOKEN_OPERADOR_LOGICO = 273,
    TOKEN_PALABRA_RESERVADA_FUNCION = 274,
    TOKEN_PALABRA_RESERVADA_SI = 275,
    TOKEN_PALABRA_RESERVADA_SINO = 276,
    TOKEN_PALABRA_RESERVADA_MIENTRAS = 277,
    TOKEN_PALABRA_RESERVADA_PARA = 278,
    TOKEN_PALABRA_RESERVADA_HAZ = 279,
    TOKEN_PALABRA_RESERVADA_REGRESA = 280,
    TOKEN_PALABRA_RESERVADA_CASOS = 281,
    TOKEN_PALABRA_RESERVADA_DETENTE = 282,
    TOKEN_PALABRA_RESERVADA_IMPRIME = 283,
    TOKEN_PALABRA_RESERVADA_CASO = 284,
    TOKEN_PALABRA_PREDETERMINADO = 285,
    TOKEN_OPERADOR_LOGICO_UNARIO = 286,
    TOKEN_OPERADOR_LOGICO_AND = 287,
    TOKEN_OPERADOR_LOGICO_OR = 288,
    TOKEN_OPERADOR_MATEMATICO_MAS = 289,
    TOKEN_OPERADOR_MATEMATICO_MENOS = 290,
    TOKEN_OPERADOR_MATEMATICO_POR = 291,
    TOKEN_OPERADOR_MATEMATICO_ENTRE = 292,
    TOKEN_OPERADOR_MATEMATICO_MODULO = 293,
    TOKEN_OPERADOR_LOGICO_NOT = 294,
    TOKEN_OPERADOR_ASIGNACION = 295,
    TOKEN_PALABRA_RESERVADA_SIX = 296
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 22 "../src/gramatica.y" /* yacc.c:1910  */
 
	type_val valor;
	type_codigo codigo;
	type_cond cond;
	type_dec dec;
	type_caso caso;
	type_args args;
	type_params params;
	unsigned int id_tipo;
	unsigned int niveles;
	char* str;

#line 109 "gramatica.tab.h" /* yacc.c:1910  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_GRAMATICA_TAB_H_INCLUDED  */
