/* A Bison parser, made by GNU Bison 3.0.5.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.5"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "../src/gramatica.y" /* yacc.c:339  */

	/**
	* Analizador sintáctico/semántico implementado por:
	* - Cárdenas Vallarta Josué Rodrigo
	* - Madrigal Ramírez José Manuel
	* - Pablo Martínez Yessica Janeth
	*/
	#include "../src/tabs.h"
	#include "../src/gramatica_utils.h"
	#include <stdio.h>
	#include <stdlib.h>
	#include <stdbool.h>  
	extern int yylex();
	extern int yyparse();
	extern FILE *yyin;
	extern FILE *yyout;
	extern int yylineno;
	int yylex();
	void yyerror(char *s);

#line 87 "gramatica.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "gramatica.tab.h".  */
#ifndef YY_YY_GRAMATICA_TAB_H_INCLUDED
# define YY_YY_GRAMATICA_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    TOKEN_ENTERO = 258,
    TOKEN_FLOTANTE = 259,
    TOKEN_CADENA = 260,
    TOKEN_ID = 261,
    TOKEN_CARACTER = 262,
    TOKEN_DOBLE_PRECISION = 263,
    TOKEN_VACIO = 264,
    TOKEN_PALABRA_RESERVADA_FALSO = 265,
    TOKEN_PALABRA_RESERVADA_CIERTO = 266,
    TOKEN_OPERADOR_RELACIONAL = 267,
    TOKEN_TIPO = 268,
    TOKEN_TIPO_REGISTRO = 269,
    TOKEN_TIPO_FUNCION = 270,
    TOKEN_TIPO_PARAM = 271,
    TOKEN_ERROR = 272,
    TOKEN_OPERADOR_LOGICO = 273,
    TOKEN_PALABRA_RESERVADA_FUNCION = 274,
    TOKEN_PALABRA_RESERVADA_SI = 275,
    TOKEN_PALABRA_RESERVADA_SINO = 276,
    TOKEN_PALABRA_RESERVADA_MIENTRAS = 277,
    TOKEN_PALABRA_RESERVADA_PARA = 278,
    TOKEN_PALABRA_RESERVADA_HAZ = 279,
    TOKEN_PALABRA_RESERVADA_REGRESA = 280,
    TOKEN_PALABRA_RESERVADA_CASOS = 281,
    TOKEN_PALABRA_RESERVADA_DETENTE = 282,
    TOKEN_PALABRA_RESERVADA_IMPRIME = 283,
    TOKEN_PALABRA_RESERVADA_CASO = 284,
    TOKEN_PALABRA_PREDETERMINADO = 285,
    TOKEN_OPERADOR_LOGICO_UNARIO = 286,
    TOKEN_OPERADOR_LOGICO_AND = 287,
    TOKEN_OPERADOR_LOGICO_OR = 288,
    TOKEN_OPERADOR_MATEMATICO_MAS = 289,
    TOKEN_OPERADOR_MATEMATICO_MENOS = 290,
    TOKEN_OPERADOR_MATEMATICO_POR = 291,
    TOKEN_OPERADOR_MATEMATICO_ENTRE = 292,
    TOKEN_OPERADOR_MATEMATICO_MODULO = 293,
    TOKEN_OPERADOR_LOGICO_NOT = 294,
    TOKEN_OPERADOR_ASIGNACION = 295,
    TOKEN_PALABRA_RESERVADA_SIX = 296
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 22 "../src/gramatica.y" /* yacc.c:355  */
 
	type_val valor;
	type_codigo codigo;
	type_cond cond;
	type_dec dec;
	type_caso caso;
	type_args args;
	type_params params;
	unsigned int id_tipo;
	unsigned int niveles;
	char* str;

#line 182 "gramatica.tab.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_GRAMATICA_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 199 "gramatica.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  3
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   183

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  51
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  21
/* YYNRULES -- Number of rules.  */
#define YYNRULES  63
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  147

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   296

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      46,    47,     2,     2,    43,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    50,    42,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    44,     2,    45,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    48,     2,    49,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    80,    80,    80,    88,    92,    94,    97,   102,   108,
     108,   110,   125,   127,   131,   136,   151,   168,   168,   175,
     185,   191,   206,   231,   253,   274,   298,   310,   316,   321,
     326,   375,   381,   389,   424,   426,   439,   441,   452,   452,
     454,   465,   476,   487,   498,   509,   512,   515,   518,   521,
     524,   527,   535,   565,   570,   576,   585,   594,   603,   613,
     618,   621,   635,   641
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "TOKEN_ENTERO", "TOKEN_FLOTANTE",
  "TOKEN_CADENA", "TOKEN_ID", "TOKEN_CARACTER", "TOKEN_DOBLE_PRECISION",
  "TOKEN_VACIO", "TOKEN_PALABRA_RESERVADA_FALSO",
  "TOKEN_PALABRA_RESERVADA_CIERTO", "TOKEN_OPERADOR_RELACIONAL",
  "TOKEN_TIPO", "TOKEN_TIPO_REGISTRO", "TOKEN_TIPO_FUNCION",
  "TOKEN_TIPO_PARAM", "TOKEN_ERROR", "TOKEN_OPERADOR_LOGICO",
  "TOKEN_PALABRA_RESERVADA_FUNCION", "TOKEN_PALABRA_RESERVADA_SI",
  "TOKEN_PALABRA_RESERVADA_SINO", "TOKEN_PALABRA_RESERVADA_MIENTRAS",
  "TOKEN_PALABRA_RESERVADA_PARA", "TOKEN_PALABRA_RESERVADA_HAZ",
  "TOKEN_PALABRA_RESERVADA_REGRESA", "TOKEN_PALABRA_RESERVADA_CASOS",
  "TOKEN_PALABRA_RESERVADA_DETENTE", "TOKEN_PALABRA_RESERVADA_IMPRIME",
  "TOKEN_PALABRA_RESERVADA_CASO", "TOKEN_PALABRA_PREDETERMINADO",
  "TOKEN_OPERADOR_LOGICO_UNARIO", "TOKEN_OPERADOR_LOGICO_AND",
  "TOKEN_OPERADOR_LOGICO_OR", "TOKEN_OPERADOR_MATEMATICO_MAS",
  "TOKEN_OPERADOR_MATEMATICO_MENOS", "TOKEN_OPERADOR_MATEMATICO_POR",
  "TOKEN_OPERADOR_MATEMATICO_ENTRE", "TOKEN_OPERADOR_MATEMATICO_MODULO",
  "TOKEN_OPERADOR_LOGICO_NOT", "TOKEN_OPERADOR_ASIGNACION",
  "TOKEN_PALABRA_RESERVADA_SIX", "';'", "','", "'['", "']'", "'('", "')'",
  "'{'", "'}'", "':'", "$accept", "programa", "$@1", "declaraciones",
  "tipo", "lista", "arreglo", "funciones", "argumentos",
  "lista_argumentos", "parte_arreglo", "sentencias", "sentencia", "casos",
  "predeterminado", "parte_izquierda", "var_arreglo", "expresion",
  "parametros", "lista_param", "condicion", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,    59,    44,    91,    93,    40,    41,   123,   125,
      58
};
# endif

#define YYPACT_NINF -49

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-49)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     -49,    17,    -3,   -49,   -49,   -49,    23,    32,    16,    26,
      -3,    79,   -49,   -49,    81,    94,    62,    16,    73,    16,
     -49,    -3,   -49,   108,    88,    84,    92,    89,    -3,    99,
     -49,    -3,   139,    92,    53,    92,   -49,   -49,   100,   113,
     114,    53,     8,   115,   121,   145,    53,   116,    53,   122,
     -49,    28,    28,    53,   142,   -49,   -49,   -49,    10,   -49,
     -49,   -49,   123,    87,   145,   -49,    96,   117,   -49,   -49,
     145,   -49,   -49,    28,    28,    54,   -26,   -10,   126,   124,
     145,   145,   145,   145,   145,   145,   145,   145,   -49,    68,
     -49,   -49,   105,   -49,    38,   145,    28,    28,    53,    53,
      28,    28,    27,   120,   125,   128,    75,    80,    80,   -49,
     -49,   -49,   127,   -49,   -49,   120,   -49,   -49,   148,   -49,
     -14,    61,   -49,   -49,   145,   -49,   144,    53,    53,   132,
     120,   129,   146,   -49,   130,   -49,   175,   131,   133,    53,
      53,    53,   -49,   -49,   144,   -49,   -49
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       2,     0,     5,     1,     6,    12,     0,     3,    10,     0,
       0,     0,     8,     4,     0,     0,     0,    10,     0,    10,
       7,    14,     9,     0,     0,    13,    18,     0,     0,     0,
      16,     5,     0,    18,     0,    18,    17,    37,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    20,     0,
      15,     0,     0,     0,     0,    47,    48,    46,    51,    50,
      49,    28,    45,     0,     0,    31,     0,     0,    11,    19,
       0,    63,    62,     0,     0,     0,     0,     0,     0,     0,
       0,    53,     0,     0,     0,     0,     0,     0,    27,     0,
      32,    29,     0,    59,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    56,     0,    54,     0,    40,    41,    42,
      43,    44,     0,    26,    60,    61,    57,    58,    21,    23,
       0,     0,    38,    52,     0,    39,    34,     0,     0,     0,
      55,     0,    36,    22,     0,    24,     0,     0,     0,     0,
       0,     0,    30,    25,    34,    35,    33
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
     -49,   -49,   -49,   149,    -1,   -49,   109,   -49,   -49,   -49,
      64,   -45,   -41,    39,   -49,   -49,   -49,   -40,   -49,   -49,
     -48
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     2,     5,     6,     9,    12,     7,    24,    25,
      30,    47,    48,   132,   138,    49,    62,    75,   104,   105,
      76
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      54,    67,    63,    69,    77,    66,    96,    97,    78,    15,
       4,    55,    56,    57,    58,    59,    60,     3,    96,    97,
      23,    98,    96,    97,    89,    93,    94,    32,   128,     8,
      92,    55,    56,    57,    58,    59,    60,    99,    71,    72,
     102,   103,   106,   107,   108,   109,   110,   111,   116,   117,
      61,    10,   120,   121,    80,   115,    81,   118,   119,    37,
      11,    83,    84,    85,    86,    87,    95,    73,    13,    14,
      96,    97,   122,    38,    74,    39,    40,    41,    42,    43,
      44,    45,    16,   134,   130,   114,   133,    17,    83,    84,
      85,    86,    87,    96,    97,   144,   145,    36,   143,    50,
      18,    46,    83,    84,    85,    86,    87,    19,   129,    83,
      84,    85,    86,    87,    26,   112,    85,    86,    87,    21,
     125,    83,    84,    85,    86,    87,    20,    28,    22,    88,
      83,    84,    85,    86,    87,    27,    29,    31,    90,    83,
      84,    85,    86,    87,    33,    35,    51,   113,    55,    56,
      57,    58,    59,    60,    83,    84,    85,    86,    87,    52,
      53,    64,    70,    65,    79,    68,    91,    82,   100,   127,
     101,   124,   123,   131,   135,   126,   137,   139,   140,   136,
      34,   141,   142,   146
};

static const yytype_uint8 yycheck[] =
{
      41,    46,    42,    48,    52,    45,    32,    33,    53,    10,
      13,     3,     4,     5,     6,     7,     8,     0,    32,    33,
      21,    47,    32,    33,    64,    73,    74,    28,    42,     6,
      70,     3,     4,     5,     6,     7,     8,    47,    10,    11,
      80,    81,    82,    83,    84,    85,    86,    87,    96,    97,
      42,    19,   100,   101,    44,    95,    46,    98,    99,     6,
      44,    34,    35,    36,    37,    38,    12,    39,    42,    43,
      32,    33,    45,    20,    46,    22,    23,    24,    25,    26,
      27,    28,     3,   128,   124,    47,   127,     6,    34,    35,
      36,    37,    38,    32,    33,   140,   141,    33,   139,    35,
       6,    48,    34,    35,    36,    37,    38,    45,    47,    34,
      35,    36,    37,    38,     6,    47,    36,    37,    38,    46,
      45,    34,    35,    36,    37,    38,    17,    43,    19,    42,
      34,    35,    36,    37,    38,    47,    44,    48,    42,    34,
      35,    36,    37,    38,    45,     6,    46,    42,     3,     4,
       5,     6,     7,     8,    34,    35,    36,    37,    38,    46,
      46,    46,    40,    42,    22,    49,    49,    44,    42,    21,
      46,    43,    47,    29,    42,    48,    30,    47,     3,    50,
      31,    50,    49,   144
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    52,    53,     0,    13,    54,    55,    58,     6,    56,
      19,    44,    57,    42,    43,    55,     3,     6,     6,    45,
      57,    46,    57,    55,    59,    60,     6,    47,    43,    44,
      61,    48,    55,    45,    54,     6,    61,     6,    20,    22,
      23,    24,    25,    26,    27,    28,    48,    62,    63,    66,
      61,    46,    46,    46,    63,     3,     4,     5,     6,     7,
       8,    42,    67,    68,    46,    42,    68,    62,    49,    62,
      40,    10,    11,    39,    46,    68,    71,    71,    62,    22,
      44,    46,    44,    34,    35,    36,    37,    38,    42,    68,
      42,    49,    68,    71,    71,    12,    32,    33,    47,    47,
      42,    46,    68,    68,    69,    70,    68,    68,    68,    68,
      68,    68,    47,    42,    47,    68,    71,    71,    63,    63,
      71,    71,    45,    47,    43,    45,    48,    21,    42,    47,
      68,    29,    64,    63,    62,    42,    50,    30,    65,    47,
       3,    50,    49,    63,    62,    62,    64
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    51,    53,    52,    54,    54,    55,    56,    56,    57,
      57,    58,    58,    59,    59,    60,    60,    61,    61,    62,
      62,    63,    63,    63,    63,    63,    63,    63,    63,    63,
      63,    63,    63,    64,    64,    65,    65,    66,    67,    67,
      68,    68,    68,    68,    68,    68,    68,    68,    68,    68,
      68,    68,    68,    69,    69,    70,    70,    71,    71,    71,
      71,    71,    71,    71
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     3,     3,     0,     1,     4,     2,     4,
       0,    11,     0,     1,     0,     5,     3,     3,     0,     2,
       1,     5,     7,     5,     7,     9,     4,     3,     2,     3,
       8,     2,     3,     5,     0,     3,     0,     1,     4,     4,
       3,     3,     3,     3,     3,     1,     1,     1,     1,     1,
       1,     1,     4,     0,     1,     3,     1,     3,     3,     2,
       3,     3,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 80 "../src/gramatica.y" /* yacc.c:1648  */
    {setup_tabs();}
#line 1395 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 3:
#line 81 "../src/gramatica.y" /* yacc.c:1648  */
    {
				if(!ts_existe("main")||*(ts_id_tipo_base("main"))!=tt_id_tipo(TOKEN_TIPO_FUNCION)){
					yyerror("Error semántico, se requiere una función 'main' para que se pueda compilar el archivo.");
				}
				fprintf(yyout,"%s",(yyvsp[0].codigo).code);
}
#line 1406 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 4:
#line 88 "../src/gramatica.y" /* yacc.c:1648  */
    {
									for(int n=0;n<(yyvsp[-1].dec).n_ids;n++){
										ts_insertar((yyvsp[-1].dec).ids_list[n],(yyvsp[-2].id_tipo),NULL,NULL,0);
									}
							   }
#line 1416 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 6:
#line 94 "../src/gramatica.y" /* yacc.c:1648  */
    {(yyval.id_tipo)=(yyvsp[0].id_tipo);}
#line 1422 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 7:
#line 97 "../src/gramatica.y" /* yacc.c:1648  */
    {
				(yyval.dec).n_ids=(yyvsp[-3].dec).n_ids;
				append_to_lista_str((yyvsp[-3].dec).ids_list,(yyval.dec).n_ids++,(yyvsp[-1].valor).direccion);
				(yyval.dec).ids_list=(yyvsp[-3].dec).ids_list;
		}
#line 1432 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 8:
#line 102 "../src/gramatica.y" /* yacc.c:1648  */
    {
				(yyval.dec).ids_list=malloc(sizeof(char*));
				(yyval.dec).ids_list[0]=(yyvsp[-1].valor).direccion;
				(yyval.dec).n_ids=1;
		}
#line 1442 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 11:
#line 110 "../src/gramatica.y" /* yacc.c:1648  */
    {//Falta crear ámbitos
				if(!ts_existe((yyvsp[-7].valor).direccion)){
					unsigned int t_base=tt_id_tipo(TOKEN_TIPO_FUNCION);
					ts_insertar((yyvsp[-7].valor).direccion,(yyvsp[-8].id_tipo),&t_base,(yyvsp[-5].args).lista_tipo_id,(yyvsp[-5].args).num);
					char *tmp=concat((yyvsp[-7].valor).direccion,": ");
					tmp=concat(tmp,(yyvsp[-1].codigo).code);
					(yyval.codigo).code=concat(tmp,"\n\n");
					if((yyvsp[-8].id_tipo)!=(yyvsp[-1].codigo).id_tipo){
						yyerror("Los tipos no coinciden.");
					}
				}else{
					yyerror("Id duplicado.");
				}
				(yyval.codigo).code=concat((yyvsp[-10].codigo).code,(yyval.codigo).code);
			}
#line 1462 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 12:
#line 125 "../src/gramatica.y" /* yacc.c:1648  */
    {(yyval.codigo).code="";}
#line 1468 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 13:
#line 127 "../src/gramatica.y" /* yacc.c:1648  */
    {
					(yyval.args).lista_tipo_id=(yyvsp[0].args).lista_tipo_id;
					(yyval.args).num=(yyvsp[0].args).num;
			}
#line 1477 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 14:
#line 131 "../src/gramatica.y" /* yacc.c:1648  */
    {
					(yyval.args).lista_tipo_id=malloc(0);
					(yyval.args).num=0;
			}
#line 1486 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 15:
#line 136 "../src/gramatica.y" /* yacc.c:1648  */
    {
						if((yyvsp[0].niveles)>0){

						}
						(yyvsp[-1].valor).id_tipo=(yyvsp[-2].id_tipo);
						if(!ts_existe((yyvsp[-1].valor).direccion)){
							unsigned int t_base=tt_id_tipo(TOKEN_TIPO_PARAM);
							ts_insertar((yyvsp[-1].valor).direccion,(yyvsp[-1].valor).id_tipo,&t_base,NULL,0);
							append_to_lista_args((yyvsp[-4].args).lista_tipo_id,(yyvsp[-4].args).num++,(yyvsp[-2].id_tipo));
							(yyval.args).lista_tipo_id=(yyvsp[-4].args).lista_tipo_id;
							(yyval.args).num=(yyvsp[-4].args).num;
						}else{
							yyerror("El identificador ya existe.");
						}
					}
#line 1506 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 16:
#line 151 "../src/gramatica.y" /* yacc.c:1648  */
    {
								if((yyvsp[0].niveles)>0){

								}
								(yyvsp[-1].valor).id_tipo=(yyvsp[-2].id_tipo);
								if(!ts_existe((yyvsp[-1].valor).direccion)){
									unsigned int t_base=tt_id_tipo(TOKEN_TIPO_PARAM);
									ts_insertar((yyvsp[-1].valor).direccion,(yyvsp[-2].id_tipo),&t_base,NULL,0);
									(yyval.args).lista_tipo_id=malloc(0);
									append_to_lista_args((yyval.args).lista_tipo_id,0,(yyvsp[-1].valor).id_tipo);
									(yyval.args).num=1;
								}else{
									yyerror("El Id ya existe.");
								}
								
						}
#line 1527 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 17:
#line 168 "../src/gramatica.y" /* yacc.c:1648  */
    {(yyval.niveles)=++(yyvsp[0].niveles);}
#line 1533 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 18:
#line 168 "../src/gramatica.y" /* yacc.c:1648  */
    {(yyval.niveles)=0;}
#line 1539 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 19:
#line 175 "../src/gramatica.y" /* yacc.c:1648  */
    {
					(yyval.codigo).code=concat((yyvsp[-1].codigo).code,(yyvsp[0].codigo).code);
					if((yyvsp[-1].codigo).next==NULL || (yyvsp[-1].codigo).next[0]=='\0'){
						(yyval.codigo).next=(yyvsp[0].codigo).next;
					}else{
						char *tmp=concat((yyvsp[-1].codigo).next," : ");
						(yyval.codigo).next=concat(tmp,(yyvsp[0].codigo).next);
					}
					(yyval.codigo).id_tipo=(yyvsp[0].codigo).id_tipo;
			}
#line 1554 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 20:
#line 185 "../src/gramatica.y" /* yacc.c:1648  */
    {
					(yyval.codigo).code=(yyvsp[0].codigo).code;
					(yyval.codigo).next=(yyvsp[0].codigo).next;
					(yyval.codigo).id_tipo=(yyvsp[0].codigo).id_tipo;
			}
#line 1564 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 21:
#line 191 "../src/gramatica.y" /* yacc.c:1648  */
    {
									if((yyval.codigo).next==NULL || (yyval.codigo).next[0]=='\0'){
										(yyval.codigo).next=new_label();
									}
									(yyvsp[0].codigo).next=(yyval.codigo).next;
									(yyvsp[-2].cond).b_true=new_label();
									(yyvsp[-2].cond).b_false=(yyval.codigo).next;
									char *tmp=concat((yyvsp[-2].cond).code,(yyvsp[-2].cond).b_true);
									tmp=concat(tmp," : ");
									tmp=concat(tmp,(yyvsp[0].codigo).code);
									tmp=concat(tmp,"\n");
									tmp=concat(tmp,(yyval.codigo).next);
									(yyval.codigo).code=concat(tmp," : ");
									(yyval.codigo).id_tipo=(yyvsp[0].codigo).id_tipo;
			}
#line 1584 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 22:
#line 206 "../src/gramatica.y" /* yacc.c:1648  */
    {
									if((yyval.codigo).next==NULL || (yyval.codigo).next[0]=='\0'){
										(yyval.codigo).next=new_label();
									}
									(yyvsp[-2].codigo).next=(yyval.codigo).next;
									(yyvsp[0].codigo).next=(yyval.codigo).next;
									char *tmp=concat((yyvsp[-4].cond).code,(yyvsp[-4].cond).b_true);
									tmp=concat(tmp," : ");
									tmp=concat(tmp,(yyvsp[-2].codigo).code);
									
									tmp=concat(tmp,"goto ");
									tmp=concat(tmp,(yyvsp[-2].codigo).next);
									tmp=concat(tmp,"\n");
									tmp=concat(tmp,(yyvsp[-4].cond).b_false);
									tmp=concat(tmp," : ");
									tmp=concat(tmp,(yyvsp[0].codigo).code);
									tmp=concat(tmp,"\n");
									tmp=concat(tmp,(yyvsp[0].codigo).next);
									(yyval.codigo).code=concat(tmp," : ");
									if((yyvsp[-2].codigo).id_tipo==tt_id_tipo(TOKEN_VACIO)){
										(yyval.codigo).id_tipo=(yyvsp[0].codigo).id_tipo;
									}else{
										(yyval.codigo).id_tipo=(yyvsp[-2].codigo).id_tipo;
									}
			}
#line 1614 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 23:
#line 231 "../src/gramatica.y" /* yacc.c:1648  */
    {
									char *tmp;
									if((yyvsp[0].codigo).next!=NULL && (yyvsp[0].codigo).next[0]!='\0'){
										tmp=concat((yyvsp[0].codigo).next," : ");
										(yyval.codigo).next=concat(tmp,(yyvsp[-2].cond).b_false);
									}else{
										(yyval.codigo).next=(yyvsp[-2].cond).b_false;
									}
									(yyvsp[0].codigo).next=new_label();

									tmp=concat((yyvsp[0].codigo).next," : ");
									tmp=concat(tmp,(yyvsp[-2].cond).code);
									tmp=concat(tmp,(yyvsp[-2].cond).b_true);
									tmp=concat(tmp," : ");
									tmp=concat(tmp,(yyvsp[0].codigo).code);
									tmp=concat(tmp,"goto ");
									tmp=concat(tmp,(yyvsp[0].codigo).next);
									tmp=concat(tmp,"\n");
									tmp=concat(tmp,(yyval.codigo).next);
									(yyval.codigo).code=concat(tmp," : ");
									(yyval.codigo).id_tipo=(yyvsp[0].codigo).id_tipo;
			}
#line 1641 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 24:
#line 253 "../src/gramatica.y" /* yacc.c:1648  */
    {
									char *tmp;
									if((yyvsp[-5].codigo).next!=NULL && (yyvsp[-5].codigo).next[0]!='\0'){
										tmp=concat((yyvsp[-5].codigo).next," : ");
										(yyval.codigo).next=concat(tmp,(yyvsp[-2].cond).b_false);
									}else{
										(yyval.codigo).next=(yyvsp[-2].cond).b_false;
									}
									(yyvsp[-5].codigo).next=new_label();
									tmp=concat((yyvsp[-5].codigo).next," : ");
									tmp=concat(tmp,(yyvsp[-5].codigo).code);
									tmp=concat(tmp,(yyvsp[-2].cond).code);
									tmp=concat(tmp,(yyvsp[-2].cond).b_true);
									tmp=concat(tmp," : ");
									tmp=concat(tmp,"goto ");
									tmp=concat(tmp,(yyvsp[-5].codigo).next);
									tmp=concat(tmp,"\n");
									tmp=concat(tmp,(yyval.codigo).next);
									(yyval.codigo).code=concat(tmp," : ");
									(yyval.codigo).id_tipo=(yyvsp[-5].codigo).id_tipo;
			}
#line 1667 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 25:
#line 274 "../src/gramatica.y" /* yacc.c:1648  */
    {
									char *tmp;
									if((yyvsp[0].codigo).next!=NULL && (yyvsp[0].codigo).next[0]!='\0'){
										tmp=concat((yyvsp[0].codigo).next," : ");
										(yyval.codigo).next=concat(tmp,(yyvsp[-4].cond).b_false);
									}else{
										(yyval.codigo).next=(yyvsp[-4].cond).b_false;
									}
									(yyvsp[0].codigo).next=new_label();

									tmp=concat((yyvsp[-6].codigo).code,(yyvsp[0].codigo).next);
									tmp=concat(tmp," : ");
									tmp=concat(tmp,(yyvsp[-4].cond).code);
									tmp=concat(tmp,(yyvsp[-4].cond).b_true);
									tmp=concat(tmp," : ");
									tmp=concat(tmp,(yyvsp[0].codigo).code);
									tmp=concat(tmp,(yyvsp[-2].codigo).code);
									tmp=concat(tmp,"goto ");
									tmp=concat(tmp,(yyvsp[0].codigo).next);
									tmp=concat(tmp,"\n");
									tmp=concat(tmp,(yyval.codigo).next);
									(yyval.codigo).code=concat(tmp," : ");
									(yyval.codigo).id_tipo=(yyvsp[0].codigo).id_tipo;
			}
#line 1696 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 26:
#line 298 "../src/gramatica.y" /* yacc.c:1648  */
    {
									if((yyvsp[-3].valor).id_tipo==(yyvsp[-1].valor).id_tipo){
										char *tmp=(yyval.codigo).code=concat((yyvsp[-1].valor).code,(yyvsp[-3].valor).direccion);
										tmp=concat(tmp,"=");
										tmp=concat(tmp,(yyvsp[-1].valor).direccion);
										(yyval.codigo).code=concat(tmp,"\n");
									}else{
										yyerror("Los tipos no coinciden.");
									}
									(yyval.codigo).next="";
									(yyval.codigo).id_tipo=tt_id_tipo(TOKEN_VACIO);
			}
#line 1713 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 27:
#line 310 "../src/gramatica.y" /* yacc.c:1648  */
    {
									(yyval.codigo).id_tipo=(yyvsp[-1].valor).id_tipo;
									char *tmp=concat((yyvsp[-1].valor).code, "return ");
									tmp=concat(tmp,(yyvsp[-1].valor).direccion);
									(yyval.codigo).code=concat(tmp,"\n");
			}
#line 1724 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 28:
#line 316 "../src/gramatica.y" /* yacc.c:1648  */
    {
									(yyval.codigo).id_tipo=tt_id_tipo(TOKEN_VACIO);
									(yyval.codigo).code="return";
									(yyval.codigo).next="";
			}
#line 1734 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 29:
#line 321 "../src/gramatica.y" /* yacc.c:1648  */
    {
									(yyval.codigo).next=(yyvsp[-1].codigo).next;
									(yyval.codigo).code=(yyvsp[-1].codigo).code;
									(yyval.codigo).id_tipo=(yyvsp[-1].codigo).id_tipo;
			}
#line 1744 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 30:
#line 326 "../src/gramatica.y" /* yacc.c:1648  */
    {
									char *tmp;
									(yyval.codigo).code="";
									(yyval.codigo).next="";
									for(int n=(yyvsp[-2].caso).n_casos-1;n>-1;n--){
										char *label_t=new_label(),*label_f=new_label();
										tmp=concat((yyval.codigo).code,"if ");
										tmp=concat(tmp,(yyvsp[-5].valor).direccion);
										tmp=concat(tmp," == ");
										tmp=concat(tmp,(yyvsp[-2].caso).list_comparacion[n]);

										tmp=concat(tmp," goto ");
										tmp=concat(tmp,label_t);
										tmp=concat(tmp,"\ngoto ");
										tmp=concat(tmp,label_f);

										tmp=concat(tmp,"\n");
										tmp=concat(tmp,label_t);
										tmp=concat(tmp," : ");

										(yyval.codigo).code=concat(tmp,(yyvsp[-2].caso).list_code[n]);

										if((yyvsp[-2].caso).list_next[n]!=NULL && (yyvsp[-2].caso).list_next[n][0]!='\0'){
											(yyval.codigo).next=concat((yyval.codigo).next,(yyvsp[-2].caso).list_next[n]);
											(yyval.codigo).next=concat((yyval.codigo).next,": ");
										}else{
											if(n>0){
												tmp=concat((yyval.codigo).code,"goto ");
												tmp=concat(tmp,(yyvsp[-2].caso).list_actual_label[n-1]);
												(yyval.codigo).code=concat(tmp,"\n");
											}
										}
										(yyval.codigo).code=concat((yyval.codigo).code,label_f);
										(yyval.codigo).code=concat((yyval.codigo).code,": ");
										(yyval.codigo).code=concat((yyval.codigo).code,"\n");
									}
									if((yyvsp[-1].caso).n_casos>0){
										(yyval.codigo).code=concat((yyval.codigo).code,(yyvsp[-1].caso).list_code[0]);
									}

									if((yyval.codigo).next!=NULL && (yyval.codigo).next[0]!='\0'){
										(yyval.codigo).code=concat((yyval.codigo).code,(yyval.codigo).next);
									}
									if((yyvsp[-2].caso).id_tipo==tt_id_tipo(TOKEN_VACIO)){
										(yyval.codigo).id_tipo=(yyvsp[-1].caso).id_tipo;
									}else{
										(yyval.codigo).id_tipo=(yyvsp[-2].caso).id_tipo;
									}
			}
#line 1798 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 31:
#line 375 "../src/gramatica.y" /* yacc.c:1648  */
    {
									(yyval.codigo).next=new_label();
									char *tmp=concat("goto ",(yyval.codigo).next);
									(yyval.codigo).code=concat(tmp,"\n");
									(yyval.codigo).id_tipo=tt_id_tipo(TOKEN_VACIO);
			}
#line 1809 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 32:
#line 381 "../src/gramatica.y" /* yacc.c:1648  */
    {
				char *tmp=concat((yyvsp[-1].valor).code,"imprime ");
				tmp=concat(tmp,(yyvsp[-1].valor).direccion);
				tmp=concat(tmp,"\n");
				(yyval.codigo).code=tmp;
				(yyval.codigo).id_tipo=tt_id_tipo(TOKEN_VACIO);
			}
#line 1821 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 33:
#line 389 "../src/gramatica.y" /* yacc.c:1648  */
    {
				if((yyvsp[0].caso).n_casos==0){
						(yyval.caso).list_next=malloc(sizeof(char*));
						(yyval.caso).list_next[0]=(yyvsp[-1].codigo).next;
						(yyval.caso).list_actual_label=malloc(sizeof(char*));
						(yyval.caso).list_actual_label[0]=new_label();
						(yyval.caso).list_code=malloc(sizeof(char*));
						(yyval.caso).list_code[0]=concat((yyval.caso).list_actual_label[0]," : ");
						(yyval.caso).list_code[0]=concat((yyval.caso).list_code[0],(yyvsp[-1].codigo).code);
						(yyval.caso).list_comparacion=malloc(sizeof(char*));
						(yyval.caso).list_comparacion[0]=(yyvsp[-2].valor).direccion;
						(yyval.caso).n_casos=1;
				}else{
						(yyval.caso).n_casos=(yyvsp[0].caso).n_casos;
						append_to_lista_str((yyvsp[0].caso).list_next,(yyval.caso).n_casos,(yyvsp[-1].codigo).next);
						(yyval.caso).list_next=(yyvsp[0].caso).list_next;
						append_to_lista_str((yyvsp[0].caso).list_actual_label,(yyval.caso).n_casos,new_label());
						(yyval.caso).list_actual_label=(yyvsp[0].caso).list_actual_label;

						append_to_lista_str((yyvsp[0].caso).list_code,(yyval.caso).n_casos,(yyvsp[-1].codigo).code);
						(yyval.caso).list_code=(yyvsp[0].caso).list_code;

						(yyval.caso).list_code[(yyval.caso).n_casos]=concat((yyval.caso).list_actual_label[(yyval.caso).n_casos]," : ");
						(yyval.caso).list_code[(yyval.caso).n_casos]=concat((yyval.caso).list_code[(yyval.caso).n_casos],(yyvsp[-1].codigo).code);

						append_to_lista_str((yyvsp[0].caso).list_comparacion,(yyval.caso).n_casos,(yyvsp[-2].valor).direccion);
						(yyval.caso).list_comparacion=(yyvsp[0].caso).list_comparacion;
						(yyval.caso).n_casos++;
				}
				if((yyvsp[0].caso).id_tipo==tt_id_tipo(TOKEN_VACIO)){
					(yyval.caso).id_tipo=(yyvsp[-1].codigo).id_tipo;
				}else{
					(yyval.caso).id_tipo=(yyvsp[0].caso).id_tipo;
				}
		}
#line 1861 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 34:
#line 424 "../src/gramatica.y" /* yacc.c:1648  */
    {(yyval.caso).n_casos=0;(yyval.caso).id_tipo=tt_id_tipo(TOKEN_VACIO);}
#line 1867 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 35:
#line 426 "../src/gramatica.y" /* yacc.c:1648  */
    {
						(yyval.caso).list_next=malloc(sizeof(char*));
						(yyval.caso).list_next[0]=(yyvsp[0].codigo).next;
						(yyval.caso).list_actual_label=malloc(sizeof(char*));
						(yyval.caso).list_actual_label[0]=new_label();
						(yyval.caso).list_code=malloc(sizeof(char*));
						(yyval.caso).list_code[0]=concat((yyval.caso).list_actual_label[0]," : ");
						(yyval.caso).list_code[0]=concat((yyval.caso).list_code[0],(yyvsp[0].codigo).code);
						(yyval.caso).list_comparacion=malloc(sizeof(char*));
						(yyval.caso).list_comparacion[0]=NULL;
						(yyval.caso).n_casos=1;
						(yyval.caso).id_tipo=(yyvsp[0].codigo).id_tipo;
				}
#line 1885 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 36:
#line 439 "../src/gramatica.y" /* yacc.c:1648  */
    {(yyval.caso).n_casos=0;(yyval.caso).id_tipo=tt_id_tipo(TOKEN_VACIO);}
#line 1891 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 37:
#line 441 "../src/gramatica.y" /* yacc.c:1648  */
    {
						if(ts_existe((yyvsp[0].valor).direccion)){
							(yyval.valor).direccion=(yyvsp[0].valor).direccion;
							(yyval.valor).id_tipo=ts_id_tipo((yyvsp[0].valor).direccion);
						}else{
							yyerror("El identificador no existe.");
						}
					}
#line 1904 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 40:
#line 454 "../src/gramatica.y" /* yacc.c:1648  */
    {
								(yyval.valor).id_tipo=type_max_id((yyvsp[-2].valor).id_tipo,(yyvsp[0].valor).id_tipo);
								(yyval.valor).direccion=new_temp();
								char *tmp=concat((yyvsp[-2].valor).code,(yyvsp[0].valor).code);
								tmp=concat(tmp,(yyval.valor).direccion);
								tmp=concat(tmp,"=");
								tmp=concat(tmp,(yyvsp[-2].valor).direccion);
								tmp=concat(tmp,"+");
								tmp=concat(tmp,(yyvsp[0].valor).direccion);
								(yyval.valor).code=concat(tmp,"\n");
			}
#line 1920 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 41:
#line 465 "../src/gramatica.y" /* yacc.c:1648  */
    {
								(yyval.valor).id_tipo=type_max_id((yyvsp[-2].valor).id_tipo,(yyvsp[0].valor).id_tipo);
								(yyval.valor).direccion=new_temp();
								char *tmp=concat((yyvsp[-2].valor).code,(yyvsp[0].valor).code);
								tmp=concat(tmp,(yyval.valor).direccion);
								tmp=concat(tmp,"=");
								tmp=concat(tmp,(yyvsp[-2].valor).direccion);
								tmp=concat(tmp,"-");
								tmp=concat(tmp,(yyvsp[0].valor).direccion);
								(yyval.valor).code=concat(tmp,"\n");
			}
#line 1936 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 42:
#line 476 "../src/gramatica.y" /* yacc.c:1648  */
    {
								(yyval.valor).id_tipo=type_max_id((yyvsp[-2].valor).id_tipo,(yyvsp[0].valor).id_tipo);
								(yyval.valor).direccion=new_temp();
								char *tmp=concat((yyvsp[-2].valor).code,(yyvsp[0].valor).code);
								tmp=concat(tmp,(yyval.valor).direccion);
								tmp=concat(tmp,"=");
								tmp=concat(tmp,(yyvsp[-2].valor).direccion);
								tmp=concat(tmp,"*");
								tmp=concat(tmp,(yyvsp[0].valor).direccion);
								(yyval.valor).code=concat(tmp,"\n");
			}
#line 1952 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 43:
#line 487 "../src/gramatica.y" /* yacc.c:1648  */
    {
								(yyval.valor).id_tipo=type_max_id((yyvsp[-2].valor).id_tipo,(yyvsp[0].valor).id_tipo);
								(yyval.valor).direccion=new_temp();
								char *tmp=concat((yyvsp[-2].valor).code,(yyvsp[0].valor).code);
								tmp=concat(tmp,(yyval.valor).direccion);
								tmp=concat(tmp,"=");
								tmp=concat(tmp,(yyvsp[-2].valor).direccion);
								tmp=concat(tmp,"/");
								tmp=concat(tmp,(yyvsp[0].valor).direccion);
								(yyval.valor).code=concat(tmp,"\n");
			}
#line 1968 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 44:
#line 498 "../src/gramatica.y" /* yacc.c:1648  */
    {
								(yyval.valor).id_tipo=type_max_id((yyvsp[-2].valor).id_tipo,(yyvsp[0].valor).id_tipo);
								(yyval.valor).direccion=new_temp();
								char *tmp=concat((yyvsp[-2].valor).code,(yyvsp[0].valor).code);
								tmp=concat(tmp,(yyval.valor).direccion);
								tmp=concat(tmp,"=");
								tmp=concat(tmp,(yyvsp[-2].valor).direccion);
								tmp=concat(tmp,"%");
								tmp=concat(tmp,(yyvsp[0].valor).direccion);
								(yyval.valor).code=concat(tmp,"\n");
			}
#line 1984 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 45:
#line 509 "../src/gramatica.y" /* yacc.c:1648  */
    {
						(yyval.valor).direccion="ARREGLO";
			}
#line 1992 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 46:
#line 512 "../src/gramatica.y" /* yacc.c:1648  */
    {
								(yyval.valor)=(yyvsp[0].valor);
			}
#line 2000 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 47:
#line 515 "../src/gramatica.y" /* yacc.c:1648  */
    {
								(yyval.valor)=(yyvsp[0].valor);
			}
#line 2008 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 48:
#line 518 "../src/gramatica.y" /* yacc.c:1648  */
    {
								(yyval.valor)=(yyvsp[0].valor);
			}
#line 2016 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 49:
#line 521 "../src/gramatica.y" /* yacc.c:1648  */
    {
								(yyval.valor)=(yyvsp[0].valor);
			}
#line 2024 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 50:
#line 524 "../src/gramatica.y" /* yacc.c:1648  */
    {
								(yyval.valor)=(yyvsp[0].valor);
			}
#line 2032 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 51:
#line 527 "../src/gramatica.y" /* yacc.c:1648  */
    {
								if(ts_existe((yyvsp[0].valor).direccion)){
									(yyval.valor).id_tipo=(yyvsp[0].valor).id_tipo;
									(yyval.valor).direccion=(yyvsp[0].valor).direccion;
								}else{
									yyerror(concat("El identificador no existe :",(yyvsp[0].valor).direccion));
								}
			}
#line 2045 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 52:
#line 535 "../src/gramatica.y" /* yacc.c:1648  */
    {
								if(ts_existe((yyvsp[-3].valor).direccion)){
									unsigned int base=*(ts_id_tipo_base((yyvsp[-3].valor).direccion));
									if(base==tt_id_tipo(TOKEN_TIPO_FUNCION)){
										if((yyvsp[-1].params).num!=ts_n_args((yyvsp[-3].valor).direccion)){
											yyerror(concat("Se ha intentado llamar a la sig. función con un número de argumentos incorrectos ",(yyvsp[-3].valor).direccion));
										}else{
											unsigned int* args=ts_args((yyvsp[-3].valor).direccion);
											for(int n=0;n<(yyvsp[-1].params).num;n++){
												if((yyvsp[-1].params).lista_tipo_id[n]!=args[n]){
													yyerror("Se intenta llamar una función con argumentos que no coinciden con el tipo de los parámetros.");
												}
											}
											(yyval.valor).id_tipo=(yyvsp[-3].valor).id_tipo;
											(yyval.valor).direccion=new_temp();
											char *tmp=concat((yyvsp[-1].params).code,(yyval.valor).direccion);
											tmp=concat(tmp,"= call ");
											tmp=concat(tmp,(yyvsp[-3].valor).direccion);
											tmp=concat(tmp,",");
											tmp=concat(tmp,int_to_str((yyvsp[-1].params).num));
											(yyval.valor).code=concat(tmp,"\n");
										}
									}else{
										yyerror(concat((yyvsp[-3].valor).direccion," no es una función."));
									}
								}else{
									yyerror(concat("La siguiente función no existe:",(yyvsp[-3].valor).direccion));
								}
			}
#line 2079 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 53:
#line 565 "../src/gramatica.y" /* yacc.c:1648  */
    {
				(yyval.params).code="";
				(yyval.params).num=0;
				(yyval.params).lista_tipo_id=NULL;
			}
#line 2089 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 54:
#line 570 "../src/gramatica.y" /* yacc.c:1648  */
    {
				(yyval.params).code=concat((yyvsp[0].params).code,(yyvsp[0].params).code_params);
				(yyval.params).num=(yyvsp[0].params).num;
				(yyval.params).lista_tipo_id=(yyvsp[0].params).lista_tipo_id;
			}
#line 2099 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 55:
#line 576 "../src/gramatica.y" /* yacc.c:1648  */
    {
						(yyval.params).code=concat((yyvsp[-2].params).code,(yyvsp[0].valor).code);
						char *tmp=concat((yyvsp[-2].params).code_params,"param ");
						tmp=concat(tmp,(yyvsp[0].valor).direccion);
						(yyval.params).code_params=concat(tmp,"\n");
						append_to_lista_args((yyvsp[-2].params).lista_tipo_id,(yyvsp[-2].params).num++,(yyvsp[0].valor).id_tipo);
						(yyval.params).lista_tipo_id=(yyvsp[-2].params).lista_tipo_id;
						(yyval.params).num=(yyvsp[-2].params).num;
				}
#line 2113 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 56:
#line 585 "../src/gramatica.y" /* yacc.c:1648  */
    {
						(yyval.params).code=(yyvsp[0].valor).code;
						char *tmp=concat("param ",(yyvsp[0].valor).direccion);
						(yyval.params).code_params=concat(tmp,"\n");
						(yyval.params).lista_tipo_id=malloc(0);
						append_to_lista_args((yyval.params).lista_tipo_id,0,(yyvsp[0].valor).id_tipo);
						(yyval.params).num=1;
				}
#line 2126 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 57:
#line 594 "../src/gramatica.y" /* yacc.c:1648  */
    {
						(yyval.cond).b_true=(yyvsp[0].cond).b_true;
						char *tmp=concat((yyvsp[-2].cond).b_false,":");
						(yyval.cond).b_false=concat(tmp,(yyvsp[0].cond).b_false);

						tmp=concat((yyvsp[-2].cond).code,(yyvsp[-2].cond).b_true);
						tmp=concat(tmp,":");
						(yyval.cond).code=concat(tmp,(yyvsp[0].cond).code);
			}
#line 2140 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 58:
#line 603 "../src/gramatica.y" /* yacc.c:1648  */
    {
						char *tmp=concat((yyvsp[-2].cond).b_true,":");
						(yyval.cond).b_true=concat(tmp,(yyvsp[0].cond).b_true);

						(yyval.cond).b_false=(yyvsp[0].cond).b_false;

						tmp=concat((yyvsp[-2].cond).code,(yyvsp[-2].cond).b_false);
						tmp=concat(tmp,":");
						(yyval.cond).code=concat(tmp,(yyvsp[0].cond).code);
			}
#line 2155 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 59:
#line 613 "../src/gramatica.y" /* yacc.c:1648  */
    {
						(yyval.cond).b_false=(yyvsp[0].cond).b_true;
						(yyval.cond).b_true=(yyvsp[0].cond).b_false;
						(yyval.cond).code=(yyvsp[0].cond).code;
			}
#line 2165 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 60:
#line 618 "../src/gramatica.y" /* yacc.c:1648  */
    {
						(yyval.cond)=(yyvsp[-1].cond);
			}
#line 2173 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 61:
#line 621 "../src/gramatica.y" /* yacc.c:1648  */
    {
						(yyval.cond).b_true=new_label();
						(yyval.cond).b_false=new_label();
						char *tmp=concat((yyvsp[-2].valor).code,(yyvsp[0].valor).code);
						tmp=concat(tmp," if ");
						tmp=concat(tmp,(yyvsp[-2].valor).direccion);
						tmp=concat(tmp,(yyvsp[-1].str));
						tmp=concat(tmp,(yyvsp[0].valor).direccion);
						tmp=concat(tmp," goto ");
						tmp=concat(tmp,(yyval.cond).b_true);
						tmp=concat(tmp,"\ngoto ");
						tmp=concat(tmp,(yyval.cond).b_false);
						(yyval.cond).code=concat(tmp,"\n");
			}
#line 2192 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 62:
#line 635 "../src/gramatica.y" /* yacc.c:1648  */
    {
						(yyval.cond).b_true=new_label();
						(yyval.cond).b_false=new_label();
						char *tmp=concat("goto ",(yyval.cond).b_true);
						(yyval.cond).code=concat(tmp,"\n");
			}
#line 2203 "gramatica.tab.c" /* yacc.c:1648  */
    break;

  case 63:
#line 641 "../src/gramatica.y" /* yacc.c:1648  */
    {
						(yyval.cond).b_true=new_label();
						(yyval.cond).b_false=new_label();
						char *tmp=concat("goto ",(yyval.cond).b_false);
						(yyval.cond).code=concat(tmp,"\n");
			}
#line 2214 "gramatica.tab.c" /* yacc.c:1648  */
    break;


#line 2218 "gramatica.tab.c" /* yacc.c:1648  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 649 "../src/gramatica.y" /* yacc.c:1907  */

/**
*Función principal donde vamos llamando a las funciones principales del compilador.
*/
int main(int argc, char **argv){
	FILE *i;
	if(argc!=3){
		printf("La sintaxis para ejecutar el proyecto es './proyecto <Archivo Entrada> <Archivo Salida>'.\n");
		return -1;
	}
	yyin=fopen(argv[1],"r");
	if(yyin==NULL){
		printf("No se pudo leer el archivo de entrada '%s', favor de verificarlo.\n",argv[1]);
		return -1;
	}
	yyout=fopen(argv[2],"w");
	if(yyout==NULL){
		printf("No se puede escribir en el archivo de salida '%s', favor de verificarlo.\n",argv[2]);
		return -1;
	}
	int result=yyparse();
	fclose(yyin);
	fclose(yyout);
	return result;
}

/**
*Función que nos permite personalizar los mensajes de error cuando el análisis 
*sintáctico manda error.
*
*@param s Es el apuntador que contiene el texto del error.
*/
void yyerror(char *s) {
	printf("Error en el análisis sintáctico/semántico, en la linea %d: \n%s\n ", yylineno , s );
	exit(0);
}

