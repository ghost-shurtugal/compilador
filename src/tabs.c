/**
* Funciones de utilidad para el manejo las tablas de símbolos y tipos.
* Implementadas por:
* - Cárdenas Vallarta Josué Rodrigo
* - Madrigal Ramírez José Manuel
* - Pablo Martínez Yessica Janeth
*/

#include "tabs.h"
#include <stdlib.h>
#include "gramatica_utils.h"
#include "../build/gramatica.tab.h"
#include <string.h>

/**
*Función que nos permite personalizar los mensajes de error cuando el análisis 
*sintáctico manda error.
*
*@param s Es el apuntador que contiene el texto del error.
*/
extern void yyerror(char *s);

/**
* Dirección que maneja la próxima dirección disponible,
*/
unsigned int direccion_actual=0;


/**
* Tipos
*/

/**
*Stack donde se guardan las tablas de tipos.
*/
type_table_stack *stack_tipos;

/**
*Función que mete a un stack de tipos una tabla de tipos.
*@param stack Es el apuntador del stack de tipos.
*@param tabla Es la tabla de tipos a insertar.
*/
void type_table_stack_push(type_table_stack *stack, type_table *tabla){
	node_type *tmp=malloc(sizeof(node_type));
	tmp->next=stack->head;
	tmp->info=tabla;
	stack->head=tmp;
}

/**
*Función que hace un pop a un stack de tablas de tipos.
*@param stack Es al stack que le haremos pop.
*@return La tabla de tipós en el tope o se mandará un error si no fue posible.
*/
type_table* type_table_stack_pop(type_table_stack *stack){
	if(!stack->head){
		yyerror("Se intentó hacer un pop en la pila pero no hay elementos\nLa cadena NO es aceptada.\n");
	}
	type_table* result=stack->head->info;
	stack->head=stack->head->next;
	return result;
}

/**
*Función que hace un top a un stack de tablas de tipos.
*@param stack Es al stack que le haremos top.
*@return La tabla de tipós en el tope o se mandará un error si no fue posible.
*/
type_table* type_table_stack_top(type_table_stack *stack){
	if(!stack->head){
		yyerror("Se intentó hacer un top en la pila pero no hay elementos\nLa cadena NO es aceptada.\n");
	}
	return stack->head->info;
}

/**
*Función que genera un nuevo registro de tipos  partir de ciertos atributos.
*@param id Es el identificador del registro.
*@param tipo Es el tipo del registro.
*@param tipo_base Es el apuntador del tipo base del registro.
*@param tamanio Es el tamaño del registro.
*@param num_elementos Es el número de elementos del registro.
*@return El registro con los datos anteriores.
*/
type_reg type_fill_registro(unsigned int id,unsigned int tipo,
	unsigned int *tipo_base, unsigned int tamanio,unsigned int num_elementos){
	type_reg r;
	r.id=id;
	r.tipo=tipo;
	r.tipo_base=tipo_base;
	r.tipo_base=tipo_base;
	r.tamanio=tamanio;
	r.num_elementos=num_elementos;
	return r;
}

/**
*Función que inserta un registro de tipo en la tabla de tipos del tope.
*@param tipo Es el tipo del registro.
*@param tipo_base Es el tipo base del registro.
*@param tamanio Es el tamaño del registro.
*@param num_elementos Es el número de elementos del registro.
*/
void tt_insertar(unsigned int tipo,
	unsigned int *tipo_base, unsigned int tamanio,unsigned int num_elementos){
	type_table *tmp= type_table_stack_top(stack_tipos);
	unsigned int n_regs=tmp->num_regs++;

	tmp->registros=realloc(tmp->registros,sizeof(type_reg)*(n_regs+1));
	tmp->registros[n_regs]=type_fill_registro(n_regs,tipo, tipo_base,
		tamanio, num_elementos);
}

/**
*Función que regresa el identificador de un tipo a partir del tipo.
*@param tipo Es el tipo del que queremos buscar su id.
*@return El identificador del tipo.
*/
unsigned int tt_id_tipo(unsigned int tipo){
	type_table *tmp= type_table_stack_top(stack_tipos);
	unsigned int n_regs=tmp->num_regs;
	for(int n=0;n<n_regs;n++){
		if(tmp->registros[n].tipo==tipo){
			return tmp->registros[n].id;
		}
	}
	yyerror("Error, se encontró un tipo de dato no registrado...");
}

/**
*Función que regresa el tipo a partir del identificador del tipo.
*@param id_tipo Es el identificador del tipo a buscar.
*@return El tipo que tiene ese id de tipo.
*/
unsigned int tt_tipo(unsigned int id_tipo){
	return type_table_stack_top(stack_tipos)->registros[id_tipo].tipo;
}

/**
*Función que regresa el tamaño a partir del identificador del tipo.
*@param id_tipo Es el identificador del tipo a buscar.
*@return El tamaño que tiene ese id de tipo.
*/
unsigned int tt_tamanio(unsigned int id_tipo){
	return type_table_stack_top(stack_tipos)->registros[id_tipo].tamanio;
}



/**
* Símbolos
*/

/**
*Stack de símbolos del compilador.
*/
sym_table_stack *stack_syms;

/**
*Función que inserta una tabla de símbolos al stack de símbolos.
*@param stack Es el stack que vamos a afectar.
*@param tabla Es la tabla de símbolos a insertar.
*/
void sym_table_stack_push(sym_table_stack *stack, sym_table *tabla){
	node_sym *tmp=malloc(sizeof(node_sym));
	tmp->next=stack->head;
	tmp->info=tabla;
	stack->head=tmp;
}

/**
*Función que hace pop a un stack de símbolos.
*@param stack Es el stack de símbolos al que le haremos pop.
*@return La tabla de símbolos que está en el tope o error si no es posible hacer esto.
*/
sym_table* sym_table_stack_pop(sym_table_stack *stack){
	if(!stack->head){
		yyerror("Se intentó hacer un pop en la pila pero no hay elementos\nLa cadena NO es aceptada.\n");
	}
	sym_table* result=stack->head->info;
	stack->head=stack->head->next;
	return result;
}

/**
*Función que hace top a un stack de símbolos.
*@param stack Es el stack de símbolos al que le haremos top.
*@return La tabla de símbolos que está en el tope o error si no es posible hacer esto.
*/
sym_table* sym_table_stack_top(sym_table_stack *stack){
	if(!stack->head){
		yyerror("Se intentó hacer un top en la pila pero no hay elementos\nLa cadena NO es aceptada.\n");
	}
	return stack->head->info;
}

/**
*Función que busca un registro de símbolos a partir del id del símbolo.
*@param id Es el identificador a buscar.
*@return Es el registro que se encontró o NULL en otro caso.
*/
sym_reg* search_sym_reg(char *id){
	sym_table *tmp= sym_table_stack_top(stack_syms);
	unsigned int n_regs=tmp->num_regs;
	for (int i = 0; i < n_regs; ++i){
		if(strcmp(tmp->registros[i].id,id)==0){
			return &tmp->registros[i];
		}
	}
	return NULL;
}

/**
*Función que verifica si en la tabla de símbolos existe un símbolo.
*@param id Es el id del símbolo a buscar.
*@return true si es cierto o false en otro caso.
*/
bool ts_existe(char *id){
	return search_sym_reg(id)!=NULL;
}

/**
*Función que busca un identificador de tipo a partir del identificador del símbolo.
*@param id Es el identificador del símbolo a buscar.
*@return El identificador del tipo del registro con el id o un error en otro caso.
*/
unsigned int ts_id_tipo(char *id){
	sym_reg *s=search_sym_reg(id);
	if(s==NULL){
		yyerror("Se intentó buscar el identificador del tipo de un simbolo desconocido...");
	}
	return s->id_tipo;
}

/**
*Función que obtiene el número de argumentos de un símbolo a partir del id del símbolo.
*@param id Es el identificador del símbolo a buscar.
*@return El número de argumentos del símbolo que se encontró o un error en otro caso.
*/
unsigned int ts_n_args(char *id){
	sym_reg *s=search_sym_reg(id);
	if(s==NULL){
		yyerror("Se intentó buscar el número de argumentos de un simbolo desconocido...");
	}
	return s->n_args;
}

/**
*Función que obtiene los argumentos de la tabla de símbolos a partir de un id.
*@param id Es el id a buscar.
*@return Los argumentos del símbolo o un error si no se encontró el símbolo.
*/
unsigned int* ts_args(char *id){
	sym_reg *s=search_sym_reg(id);
	if(s==NULL){
		yyerror("Se intentó buscar los argumentos de un simbolo desconocido...");
	}
	return s->args;
}

/**
*Función que obtiene el identificador del tipo base de un símbolo a partir de un id.
*@param id Es el identificador del símbolo a buscar.
*@return El id del tipo base o un error si no se encuentra el símbolo.
*/
unsigned int* ts_id_tipo_base(char *id){
	sym_reg *s=search_sym_reg(id);
	if(s==NULL){
		yyerror("Se intentó buscar el identificador del tipo base de un simbolo desconocido...");
	}
	return s->id_tipo_base;
}

/**
*Función que genera un registro de símbolos con ciertos datos.
*@param posicion Es la posición de [0 - (n-1)] donde se encuentra el registro.
*@param id Es el identificador del registro.
*@param direccion Es la dirección del registro.
*@param id_tipo Es el identificador de tipo del registro.
*@param id_tipo_base Es el identificador del tipo base del registro.
*@param args Son los argumentos del registro.
*@param n_args Es el número de argumentos del registro.
*@return Es el registro del símbolo generado.
*/
sym_reg sym_fill_registro(unsigned int posicion,char* id,int direccion,
	unsigned int id_tipo, unsigned int *id_tipo_base,unsigned int *args,unsigned int n_args){
	sym_reg r;
	r.posicion=posicion;
	r.id=id;
	r.direccion=direccion;
	r.id_tipo=id_tipo;
	r.id_tipo_base=id_tipo_base;
	r.args=args;
	r.n_args=n_args;
	return r;
}

/**
*Función que sirve para insertar en la tabala de símbolos un registro,
*@param id Es el identificador del registro.
*@param id_tipo Es el identificador del tipo.
*@param id_tipo_base Es el tipo base del registro.
*@param args Son los tipos de agumentos del registro.
*@param n_args Es el número de argumentos del registro.
*/
void ts_insertar(char* id,unsigned int id_tipo,
			unsigned int *id_tipo_base,unsigned int* args,unsigned int n_args){
	sym_table *tmp= sym_table_stack_top(stack_syms);
	unsigned int n_regs=tmp->num_regs++;

	tmp->registros=realloc(tmp->registros,sizeof(sym_reg)*(n_regs+1));

	int dir=genera_direccion(id_tipo);
	tmp->registros[n_regs]=sym_fill_registro(n_regs,id, dir,
		id_tipo, id_tipo_base, args, n_args);
}


/**
*Función que genera una nueva dirección a partir de un identificador de tipo.
*@param id_tipo Es el identificador del tipo.
*@return La nueva dirección que se generó.
*/
unsigned int genera_direccion(unsigned int id_tipo){
	direccion_actual+=tt_tamanio(id_tipo);
	return direccion_actual;
}

/**
*Función que inicializa los stacks de tipos y símbolos.
*/
void setup_tabs(){
	stack_syms=malloc(sizeof(sym_table_stack));
	sym_table* st=malloc(sizeof(sym_table));
	st->num_regs=0;
	sym_table_stack_push(stack_syms, st);

	stack_tipos=malloc(sizeof(type_table_stack));
	type_table* tt=malloc(sizeof(type_table));
	tt->num_regs=0;
	type_table_stack_push(stack_tipos, tt);


	tt_insertar(TOKEN_CADENA,(unsigned int *)TOKEN_CARACTER, 0,0);
	tt_insertar(TOKEN_CARACTER,NULL, SIZE_OF_CARAC,0);
	tt_insertar(TOKEN_TIPO_FUNCION,NULL, 0,0);
	tt_insertar(TOKEN_FLOTANTE,NULL, SIZE_OF_FLOTAN,0);
	tt_insertar(TOKEN_TIPO_PARAM,NULL, 0,0);
	tt_insertar(TOKEN_DOBLE_PRECISION,NULL, SIZE_OF_DOBLE,0);
	tt_insertar(TOKEN_VACIO,NULL, 0,0);
	tt_insertar(TOKEN_ENTERO,NULL, SIZE_OF_ENT,0);
	
}