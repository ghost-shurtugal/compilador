/**
*Encabezados de las funciones de gramatica_utils.c y definición de estructuras.
* Implementadas por:
* - Cárdenas Vallarta Josué Rodrigo
* - Madrigal Ramírez José Manuel
* - Pablo Martínez Yessica Janeth
*/

#define TAM_CADENA_DE_ENTEROS 20

/**
*Estructura para las producciones de tipo valor.
*/
typedef struct _type_val{
	unsigned int id_tipo;
	char* direccion;
	char* code;
} type_val;

/**
*Estructura para las producciones de tipo declaración.
*/
typedef struct _type_dec{
	char **ids_list;
	unsigned int n_ids;
} type_dec;

/**
*Estructura para las producciones de tipo codigo.
*/
typedef struct _type_codigo{
	char *code;
	unsigned int id_tipo;
	char *next;
} type_codigo;

/**
*Estructura para las producciones de tipo casos.
*/
typedef struct _type_caso{
	char **list_code;
	char **list_comparacion;
	char **list_next;
	char **list_actual_label;
	unsigned int id_tipo;
	unsigned int n_casos;
} type_caso;

/**
*Estructura para las producciones de tipo condición.
*/
typedef struct _type_cond{
	char *b_true;
	char *b_false;
	char *code;
} type_cond;

/**
*Estructura para las producciones de tipo argumentos.
*/
typedef struct _type_args{
	unsigned int* lista_tipo_id;
	unsigned int num;
} type_args;

/**
*Estructura para las producciones de tipo parametros.
*/
typedef struct _type_params{
	char *code;
	char *code_params;
	unsigned int* lista_tipo_id;
	unsigned int num;
} type_params;


unsigned int type_max_id(unsigned int id_type1,unsigned int id_type2);

char* concat(char *s1,char *s2);

char* scpy(char *s);

char* new_label();

char* new_temp();

char* int_to_str(int val);

void append_to_lista_args( unsigned  int* lista,unsigned int num_elems,unsigned int tipo);
void append_to_lista_str(char** lista,unsigned int num_elems,char* str);