%{
	/**
	* Analizador sintáctico/semántico implementado por:
	* - Cárdenas Vallarta Josué Rodrigo
	* - Madrigal Ramírez José Manuel
	* - Pablo Martínez Yessica Janeth
	*/
	#include "../src/tabs.h"
	#include "../src/gramatica_utils.h"
	#include <stdio.h>
	#include <stdlib.h>
	#include <stdbool.h>  
	extern int yylex();
	extern int yyparse();
	extern FILE *yyin;
	extern FILE *yyout;
	extern int yylineno;
	int yylex();
	void yyerror(char *s);
%}

%union { 
	type_val valor;
	type_codigo codigo;
	type_cond cond;
	type_dec dec;
	type_caso caso;
	type_args args;
	type_params params;
	unsigned int id_tipo;
	unsigned int niveles;
	char* str;
}

%type<dec> lista
%type<id_tipo> tipo 
%type<niveles> parte_arreglo
%type<args> argumentos lista_argumentos
%type<params> parametros lista_param
%type<valor> expresion parte_izquierda
%type<cond> condicion
%type<codigo> sentencia sentencias programa  funciones
%type<caso> predeterminado casos

%token<valor> TOKEN_ENTERO TOKEN_FLOTANTE TOKEN_CADENA TOKEN_ID TOKEN_CARACTER TOKEN_DOBLE_PRECISION
%token TOKEN_VACIO
%token<cond> TOKEN_PALABRA_RESERVADA_FALSO TOKEN_PALABRA_RESERVADA_CIERTO
%token<str> TOKEN_OPERADOR_RELACIONAL
%token<id_tipo> TOKEN_TIPO 
%token TOKEN_TIPO_REGISTRO TOKEN_TIPO_FUNCION TOKEN_TIPO_PARAM
%token TOKEN_ERROR
%token TOKEN_OPERADOR_LOGICO  
%token TOKEN_PALABRA_RESERVADA_FUNCION TOKEN_PALABRA_RESERVADA_SI
%token TOKEN_PALABRA_RESERVADA_SINO TOKEN_PALABRA_RESERVADA_MIENTRAS
%token TOKEN_PALABRA_RESERVADA_PARA TOKEN_PALABRA_RESERVADA_HAZ
%token TOKEN_PALABRA_RESERVADA_REGRESA TOKEN_PALABRA_RESERVADA_CASOS
%token TOKEN_PALABRA_RESERVADA_DETENTE TOKEN_PALABRA_RESERVADA_IMPRIME
%token TOKEN_PALABRA_RESERVADA_CASO TOKEN_PALABRA_PREDETERMINADO
%token TOKEN_OPERADOR_LOGICO_UNARIO 
%token TOKEN_OPERADOR_LOGICO_AND TOKEN_OPERADOR_LOGICO_OR 
%token TOKEN_OPERADOR_MATEMATICO_MAS TOKEN_OPERADOR_MATEMATICO_MENOS 
%token TOKEN_OPERADOR_MATEMATICO_POR TOKEN_OPERADOR_MATEMATICO_ENTRE 
%token TOKEN_OPERADOR_MATEMATICO_MODULO TOKEN_OPERADOR_LOGICO_NOT
%token TOKEN_OPERADOR_ASIGNACION

%debug
%start programa

%left TOKEN_OPERADOR_MATEMATICO_MAS TOKEN_OPERADOR_MATEMATICO_MENOS
%left TOKEN_OPERADOR_MATEMATICO_POR TOKEN_OPERADOR_MATEMATICO_ENTRE TOKEN_OPERADOR_MATEMATICO_MODULO
%left TOKEN_OPERADOR_LOGICO_AND TOKEN_OPERADOR_LOGICO_OR
%left TOKEN_OPERADOR_LOGICO_NOT
%right TOKEN_OPERADOR_ASIGNACION

%nonassoc TOKEN_PALABRA_RESERVADA_SIX
%nonassoc TOKEN_PALABRA_RESERVADA_SINO

%%

programa:	{setup_tabs();}
			declaraciones  funciones {
				if(!ts_existe("main")||*(ts_id_tipo_base("main"))!=tt_id_tipo(TOKEN_TIPO_FUNCION)){
					yyerror("Error semántico, se requiere una función 'main' para que se pueda compilar el archivo.");
				}
				fprintf(yyout,"%s",$3.code);
};

declaraciones: tipo lista ';' {
									for(int n=0;n<$2.n_ids;n++){
										ts_insertar($2.ids_list[n],$1,NULL,NULL,0);
									}
							   }| ;

tipo:	TOKEN_TIPO {$$=$1;} 
		;//| TOKEN_TIPO_REGISTRO '{' declaraciones '}';  Por el momento no haremos ésto.

lista:	lista ',' TOKEN_ID arreglo {
				$$.n_ids=$1.n_ids;
				append_to_lista_str($1.ids_list,$$.n_ids++,$3.direccion);
				$$.ids_list=$1.ids_list;
		}
		| TOKEN_ID arreglo{
				$$.ids_list=malloc(sizeof(char*));
				$$.ids_list[0]=$1.direccion;
				$$.n_ids=1;
		};

arreglo:	'[' TOKEN_ENTERO ']' arreglo | ;

funciones:	funciones TOKEN_PALABRA_RESERVADA_FUNCION tipo TOKEN_ID '(' argumentos ')' '{' declaraciones sentencias '}'  {//Falta crear ámbitos
				if(!ts_existe($4.direccion)){
					unsigned int t_base=tt_id_tipo(TOKEN_TIPO_FUNCION);
					ts_insertar($4.direccion,$3,&t_base,$6.lista_tipo_id,$6.num);
					char *tmp=concat($4.direccion,": ");
					tmp=concat(tmp,$10.code);
					$$.code=concat(tmp,"\n\n");
					if($3!=$10.id_tipo){
						yyerror("Los tipos no coinciden.");
					}
				}else{
					yyerror("Id duplicado.");
				}
				$$.code=concat($1.code,$$.code);
			}
			| {$$.code="";};

argumentos:	lista_argumentos {
					$$.lista_tipo_id=$1.lista_tipo_id;
					$$.num=$1.num;
			}
			| {
					$$.lista_tipo_id=malloc(0);
					$$.num=0;
			};

lista_argumentos:	lista_argumentos ',' tipo TOKEN_ID parte_arreglo {
						if($5>0){

						}
						$4.id_tipo=$3;
						if(!ts_existe($4.direccion)){
							unsigned int t_base=tt_id_tipo(TOKEN_TIPO_PARAM);
							ts_insertar($4.direccion,$4.id_tipo,&t_base,NULL,0);
							append_to_lista_args($1.lista_tipo_id,$1.num++,$3);
							$$.lista_tipo_id=$1.lista_tipo_id;
							$$.num=$1.num;
						}else{
							yyerror("El identificador ya existe.");
						}
					}
					| tipo TOKEN_ID  parte_arreglo {
								if($3>0){

								}
								$2.id_tipo=$1;
								if(!ts_existe($2.direccion)){
									unsigned int t_base=tt_id_tipo(TOKEN_TIPO_PARAM);
									ts_insertar($2.direccion,$1,&t_base,NULL,0);
									$$.lista_tipo_id=malloc(0);
									append_to_lista_args($$.lista_tipo_id,0,$2.id_tipo);
									$$.num=1;
								}else{
									yyerror("El Id ya existe.");
								}
								
						};

parte_arreglo:	'[' ']' parte_arreglo {$$=++$3;}| {$$=0;}

/*
	La agregamos para quitar la ambigüedad original que 
	producía tener la producción 'sentencia sentencia' en 'sentencia'
	(Producía la warning:  "warning: 50 shift/reduce conflicts [-Wconflicts-sr]")
*/
sentencias: sentencia  sentencias {
					$$.code=concat($1.code,$2.code);
					if($1.next==NULL || $1.next[0]=='\0'){
						$$.next=$2.next;
					}else{
						char *tmp=concat($1.next," : ");
						$$.next=concat(tmp,$2.next);
					}
					$$.id_tipo=$2.id_tipo;
			}
			| sentencia{
					$$.code=$1.code;
					$$.next=$1.next;
					$$.id_tipo=$1.id_tipo;
			}; 

sentencia:	  TOKEN_PALABRA_RESERVADA_SI '(' condicion ')' sentencia %prec TOKEN_PALABRA_RESERVADA_SIX{
									if($$.next==NULL || $$.next[0]=='\0'){
										$$.next=new_label();
									}
									$5.next=$$.next;
									$3.b_true=new_label();
									$3.b_false=$$.next;
									char *tmp=concat($3.code,$3.b_true);
									tmp=concat(tmp," : ");
									tmp=concat(tmp,$5.code);
									tmp=concat(tmp,"\n");
									tmp=concat(tmp,$$.next);
									$$.code=concat(tmp," : ");
									$$.id_tipo=$5.id_tipo;
			}
			| TOKEN_PALABRA_RESERVADA_SI '(' condicion ')' sentencia TOKEN_PALABRA_RESERVADA_SINO sentencia{
									if($$.next==NULL || $$.next[0]=='\0'){
										$$.next=new_label();
									}
									$5.next=$$.next;
									$7.next=$$.next;
									char *tmp=concat($3.code,$3.b_true);
									tmp=concat(tmp," : ");
									tmp=concat(tmp,$5.code);
									
									tmp=concat(tmp,"goto ");
									tmp=concat(tmp,$5.next);
									tmp=concat(tmp,"\n");
									tmp=concat(tmp,$3.b_false);
									tmp=concat(tmp," : ");
									tmp=concat(tmp,$7.code);
									tmp=concat(tmp,"\n");
									tmp=concat(tmp,$7.next);
									$$.code=concat(tmp," : ");
									if($5.id_tipo==tt_id_tipo(TOKEN_VACIO)){
										$$.id_tipo=$7.id_tipo;
									}else{
										$$.id_tipo=$5.id_tipo;
									}
			}
			| TOKEN_PALABRA_RESERVADA_MIENTRAS '(' condicion ')' sentencia  {
									char *tmp;
									if($5.next!=NULL && $5.next[0]!='\0'){
										tmp=concat($5.next," : ");
										$$.next=concat(tmp,$3.b_false);
									}else{
										$$.next=$3.b_false;
									}
									$5.next=new_label();

									tmp=concat($5.next," : ");
									tmp=concat(tmp,$3.code);
									tmp=concat(tmp,$3.b_true);
									tmp=concat(tmp," : ");
									tmp=concat(tmp,$5.code);
									tmp=concat(tmp,"goto ");
									tmp=concat(tmp,$5.next);
									tmp=concat(tmp,"\n");
									tmp=concat(tmp,$$.next);
									$$.code=concat(tmp," : ");
									$$.id_tipo=$5.id_tipo;
			}
			| TOKEN_PALABRA_RESERVADA_HAZ sentencia TOKEN_PALABRA_RESERVADA_MIENTRAS '(' condicion ')' ';' {
									char *tmp;
									if($2.next!=NULL && $2.next[0]!='\0'){
										tmp=concat($2.next," : ");
										$$.next=concat(tmp,$5.b_false);
									}else{
										$$.next=$5.b_false;
									}
									$2.next=new_label();
									tmp=concat($2.next," : ");
									tmp=concat(tmp,$2.code);
									tmp=concat(tmp,$5.code);
									tmp=concat(tmp,$5.b_true);
									tmp=concat(tmp," : ");
									tmp=concat(tmp,"goto ");
									tmp=concat(tmp,$2.next);
									tmp=concat(tmp,"\n");
									tmp=concat(tmp,$$.next);
									$$.code=concat(tmp," : ");
									$$.id_tipo=$2.id_tipo;
			}
			| TOKEN_PALABRA_RESERVADA_PARA '(' sentencias ';' condicion ';' sentencias ')' sentencia {
									char *tmp;
									if($9.next!=NULL && $9.next[0]!='\0'){
										tmp=concat($9.next," : ");
										$$.next=concat(tmp,$5.b_false);
									}else{
										$$.next=$5.b_false;
									}
									$9.next=new_label();

									tmp=concat($3.code,$9.next);
									tmp=concat(tmp," : ");
									tmp=concat(tmp,$5.code);
									tmp=concat(tmp,$5.b_true);
									tmp=concat(tmp," : ");
									tmp=concat(tmp,$9.code);
									tmp=concat(tmp,$7.code);
									tmp=concat(tmp,"goto ");
									tmp=concat(tmp,$9.next);
									tmp=concat(tmp,"\n");
									tmp=concat(tmp,$$.next);
									$$.code=concat(tmp," : ");
									$$.id_tipo=$9.id_tipo;
			}
			| parte_izquierda TOKEN_OPERADOR_ASIGNACION expresion ';' {
									if($1.id_tipo==$3.id_tipo){
										char *tmp=$$.code=concat($3.code,$1.direccion);
										tmp=concat(tmp,"=");
										tmp=concat(tmp,$3.direccion);
										$$.code=concat(tmp,"\n");
									}else{
										yyerror("Los tipos no coinciden.");
									}
									$$.next="";
									$$.id_tipo=tt_id_tipo(TOKEN_VACIO);
			}
			| TOKEN_PALABRA_RESERVADA_REGRESA expresion ';' {
									$$.id_tipo=$2.id_tipo;
									char *tmp=concat($2.code, "return ");
									tmp=concat(tmp,$2.direccion);
									$$.code=concat(tmp,"\n");
			}
			| TOKEN_PALABRA_RESERVADA_REGRESA ';' {
									$$.id_tipo=tt_id_tipo(TOKEN_VACIO);
									$$.code="return";
									$$.next="";
			}
			| '{' sentencias '}'  {
									$$.next=$2.next;
									$$.code=$2.code;
									$$.id_tipo=$2.id_tipo;
			}
			| TOKEN_PALABRA_RESERVADA_CASOS '(' expresion ')' '{' casos predeterminado '}' {
									char *tmp;
									$$.code="";
									$$.next="";
									for(int n=$6.n_casos-1;n>-1;n--){
										char *label_t=new_label(),*label_f=new_label();
										tmp=concat($$.code,"if ");
										tmp=concat(tmp,$3.direccion);
										tmp=concat(tmp," == ");
										tmp=concat(tmp,$6.list_comparacion[n]);

										tmp=concat(tmp," goto ");
										tmp=concat(tmp,label_t);
										tmp=concat(tmp,"\ngoto ");
										tmp=concat(tmp,label_f);

										tmp=concat(tmp,"\n");
										tmp=concat(tmp,label_t);
										tmp=concat(tmp," : ");

										$$.code=concat(tmp,$6.list_code[n]);

										if($6.list_next[n]!=NULL && $6.list_next[n][0]!='\0'){
											$$.next=concat($$.next,$6.list_next[n]);
											$$.next=concat($$.next,": ");
										}else{
											if(n>0){
												tmp=concat($$.code,"goto ");
												tmp=concat(tmp,$6.list_actual_label[n-1]);
												$$.code=concat(tmp,"\n");
											}
										}
										$$.code=concat($$.code,label_f);
										$$.code=concat($$.code,": ");
										$$.code=concat($$.code,"\n");
									}
									if($7.n_casos>0){
										$$.code=concat($$.code,$7.list_code[0]);
									}

									if($$.next!=NULL && $$.next[0]!='\0'){
										$$.code=concat($$.code,$$.next);
									}
									if($6.id_tipo==tt_id_tipo(TOKEN_VACIO)){
										$$.id_tipo=$7.id_tipo;
									}else{
										$$.id_tipo=$6.id_tipo;
									}
			}
			| TOKEN_PALABRA_RESERVADA_DETENTE ';' {
									$$.next=new_label();
									char *tmp=concat("goto ",$$.next);
									$$.code=concat(tmp,"\n");
									$$.id_tipo=tt_id_tipo(TOKEN_VACIO);
			}
			| TOKEN_PALABRA_RESERVADA_IMPRIME expresion ';' {
				char *tmp=concat($2.code,"imprime ");
				tmp=concat(tmp,$2.direccion);
				tmp=concat(tmp,"\n");
				$$.code=tmp;
				$$.id_tipo=tt_id_tipo(TOKEN_VACIO);
			};

casos:	TOKEN_PALABRA_RESERVADA_CASO ':' TOKEN_ENTERO sentencias casos {
				if($5.n_casos==0){
						$$.list_next=malloc(sizeof(char*));
						$$.list_next[0]=$4.next;
						$$.list_actual_label=malloc(sizeof(char*));
						$$.list_actual_label[0]=new_label();
						$$.list_code=malloc(sizeof(char*));
						$$.list_code[0]=concat($$.list_actual_label[0]," : ");
						$$.list_code[0]=concat($$.list_code[0],$4.code);
						$$.list_comparacion=malloc(sizeof(char*));
						$$.list_comparacion[0]=$3.direccion;
						$$.n_casos=1;
				}else{
						$$.n_casos=$5.n_casos;
						append_to_lista_str($5.list_next,$$.n_casos,$4.next);
						$$.list_next=$5.list_next;
						append_to_lista_str($5.list_actual_label,$$.n_casos,new_label());
						$$.list_actual_label=$5.list_actual_label;

						append_to_lista_str($5.list_code,$$.n_casos,$4.code);
						$$.list_code=$5.list_code;

						$$.list_code[$$.n_casos]=concat($$.list_actual_label[$$.n_casos]," : ");
						$$.list_code[$$.n_casos]=concat($$.list_code[$$.n_casos],$4.code);

						append_to_lista_str($5.list_comparacion,$$.n_casos,$3.direccion);
						$$.list_comparacion=$5.list_comparacion;
						$$.n_casos++;
				}
				if($5.id_tipo==tt_id_tipo(TOKEN_VACIO)){
					$$.id_tipo=$4.id_tipo;
				}else{
					$$.id_tipo=$5.id_tipo;
				}
		}
		| {$$.n_casos=0;$$.id_tipo=tt_id_tipo(TOKEN_VACIO);};

predeterminado:	TOKEN_PALABRA_PREDETERMINADO ':' sentencias {
						$$.list_next=malloc(sizeof(char*));
						$$.list_next[0]=$3.next;
						$$.list_actual_label=malloc(sizeof(char*));
						$$.list_actual_label[0]=new_label();
						$$.list_code=malloc(sizeof(char*));
						$$.list_code[0]=concat($$.list_actual_label[0]," : ");
						$$.list_code[0]=concat($$.list_code[0],$3.code);
						$$.list_comparacion=malloc(sizeof(char*));
						$$.list_comparacion[0]=NULL;
						$$.n_casos=1;
						$$.id_tipo=$3.id_tipo;
				}
				| {$$.n_casos=0;$$.id_tipo=tt_id_tipo(TOKEN_VACIO);};

parte_izquierda:	TOKEN_ID {
						if(ts_existe($1.direccion)){
							$$.direccion=$1.direccion;
							$$.id_tipo=ts_id_tipo($1.direccion);
						}else{
							yyerror("El identificador no existe.");
						}
					};
					//| var_arreglo 
					//| TOKEN_ID '.' TOKEN_ID;

var_arreglo:	TOKEN_ID '[' expresion ']' | var_arreglo '[' expresion ']';

expresion:	expresion TOKEN_OPERADOR_MATEMATICO_MAS expresion {
								$$.id_tipo=type_max_id($1.id_tipo,$3.id_tipo);
								$$.direccion=new_temp();
								char *tmp=concat($1.code,$3.code);
								tmp=concat(tmp,$$.direccion);
								tmp=concat(tmp,"=");
								tmp=concat(tmp,$1.direccion);
								tmp=concat(tmp,"+");
								tmp=concat(tmp,$3.direccion);
								$$.code=concat(tmp,"\n");
			}
			| expresion TOKEN_OPERADOR_MATEMATICO_MENOS expresion {
								$$.id_tipo=type_max_id($1.id_tipo,$3.id_tipo);
								$$.direccion=new_temp();
								char *tmp=concat($1.code,$3.code);
								tmp=concat(tmp,$$.direccion);
								tmp=concat(tmp,"=");
								tmp=concat(tmp,$1.direccion);
								tmp=concat(tmp,"-");
								tmp=concat(tmp,$3.direccion);
								$$.code=concat(tmp,"\n");
			}
			| expresion TOKEN_OPERADOR_MATEMATICO_POR expresion  {
								$$.id_tipo=type_max_id($1.id_tipo,$3.id_tipo);
								$$.direccion=new_temp();
								char *tmp=concat($1.code,$3.code);
								tmp=concat(tmp,$$.direccion);
								tmp=concat(tmp,"=");
								tmp=concat(tmp,$1.direccion);
								tmp=concat(tmp,"*");
								tmp=concat(tmp,$3.direccion);
								$$.code=concat(tmp,"\n");
			}
			| expresion TOKEN_OPERADOR_MATEMATICO_ENTRE expresion {
								$$.id_tipo=type_max_id($1.id_tipo,$3.id_tipo);
								$$.direccion=new_temp();
								char *tmp=concat($1.code,$3.code);
								tmp=concat(tmp,$$.direccion);
								tmp=concat(tmp,"=");
								tmp=concat(tmp,$1.direccion);
								tmp=concat(tmp,"/");
								tmp=concat(tmp,$3.direccion);
								$$.code=concat(tmp,"\n");
			}
			| expresion TOKEN_OPERADOR_MATEMATICO_MODULO expresion {
								$$.id_tipo=type_max_id($1.id_tipo,$3.id_tipo);
								$$.direccion=new_temp();
								char *tmp=concat($1.code,$3.code);
								tmp=concat(tmp,$$.direccion);
								tmp=concat(tmp,"=");
								tmp=concat(tmp,$1.direccion);
								tmp=concat(tmp,"%");
								tmp=concat(tmp,$3.direccion);
								$$.code=concat(tmp,"\n");
			}
			| var_arreglo {
						$$.direccion="ARREGLO";
			}
			| TOKEN_CADENA {
								$$=$1;
			}
			| TOKEN_ENTERO {
								$$=$1;
			} 
			| TOKEN_FLOTANTE {
								$$=$1;
			}
			| TOKEN_DOBLE_PRECISION {
								$$=$1;
			} 
			| TOKEN_CARACTER {
								$$=$1;
			}
			| TOKEN_ID{
								if(ts_existe($1.direccion)){
									$$.id_tipo=$1.id_tipo;
									$$.direccion=$1.direccion;
								}else{
									yyerror(concat("El identificador no existe :",$1.direccion));
								}
			}
			| TOKEN_ID '(' parametros ')' {
								if(ts_existe($1.direccion)){
									unsigned int base=*(ts_id_tipo_base($1.direccion));
									if(base==tt_id_tipo(TOKEN_TIPO_FUNCION)){
										if($3.num!=ts_n_args($1.direccion)){
											yyerror(concat("Se ha intentado llamar a la sig. función con un número de argumentos incorrectos ",$1.direccion));
										}else{
											unsigned int* args=ts_args($1.direccion);
											for(int n=0;n<$3.num;n++){
												if($3.lista_tipo_id[n]!=args[n]){
													yyerror("Se intenta llamar una función con argumentos que no coinciden con el tipo de los parámetros.");
												}
											}
											$$.id_tipo=$1.id_tipo;
											$$.direccion=new_temp();
											char *tmp=concat($3.code,$$.direccion);
											tmp=concat(tmp,"= call ");
											tmp=concat(tmp,$1.direccion);
											tmp=concat(tmp,",");
											tmp=concat(tmp,int_to_str($3.num));
											$$.code=concat(tmp,"\n");
										}
									}else{
										yyerror(concat($1.direccion," no es una función."));
									}
								}else{
									yyerror(concat("La siguiente función no existe:",$1.direccion));
								}
			};
							
parametros:	{
				$$.code="";
				$$.num=0;
				$$.lista_tipo_id=NULL;
			}
			| lista_param{
				$$.code=concat($1.code,$1.code_params);
				$$.num=$1.num;
				$$.lista_tipo_id=$1.lista_tipo_id;
			};

lista_param:	lista_param ',' expresion {
						$$.code=concat($1.code,$3.code);
						char *tmp=concat($1.code_params,"param ");
						tmp=concat(tmp,$3.direccion);
						$$.code_params=concat(tmp,"\n");
						append_to_lista_args($1.lista_tipo_id,$1.num++,$3.id_tipo);
						$$.lista_tipo_id=$1.lista_tipo_id;
						$$.num=$1.num;
				}
				| expresion{
						$$.code=$1.code;
						char *tmp=concat("param ",$1.direccion);
						$$.code_params=concat(tmp,"\n");
						$$.lista_tipo_id=malloc(0);
						append_to_lista_args($$.lista_tipo_id,0,$1.id_tipo);
						$$.num=1;
				};

condicion:	condicion TOKEN_OPERADOR_LOGICO_AND condicion{
						$$.b_true=$3.b_true;
						char *tmp=concat($1.b_false,":");
						$$.b_false=concat(tmp,$3.b_false);

						tmp=concat($1.code,$1.b_true);
						tmp=concat(tmp,":");
						$$.code=concat(tmp,$3.code);
			}
			| condicion TOKEN_OPERADOR_LOGICO_OR condicion {
						char *tmp=concat($1.b_true,":");
						$$.b_true=concat(tmp,$3.b_true);

						$$.b_false=$3.b_false;

						tmp=concat($1.code,$1.b_false);
						tmp=concat(tmp,":");
						$$.code=concat(tmp,$3.code);
			}
			| TOKEN_OPERADOR_LOGICO_NOT condicion {
						$$.b_false=$2.b_true;
						$$.b_true=$2.b_false;
						$$.code=$2.code;
			}
			| '(' condicion ')' {
						$$=$2;
			}
			| expresion TOKEN_OPERADOR_RELACIONAL expresion{
						$$.b_true=new_label();
						$$.b_false=new_label();
						char *tmp=concat($1.code,$3.code);
						tmp=concat(tmp," if ");
						tmp=concat(tmp,$1.direccion);
						tmp=concat(tmp,$2);
						tmp=concat(tmp,$3.direccion);
						tmp=concat(tmp," goto ");
						tmp=concat(tmp,$$.b_true);
						tmp=concat(tmp,"\ngoto ");
						tmp=concat(tmp,$$.b_false);
						$$.code=concat(tmp,"\n");
			}
			| TOKEN_PALABRA_RESERVADA_CIERTO {
						$$.b_true=new_label();
						$$.b_false=new_label();
						char *tmp=concat("goto ",$$.b_true);
						$$.code=concat(tmp,"\n");
			}
			| TOKEN_PALABRA_RESERVADA_FALSO {
						$$.b_true=new_label();
						$$.b_false=new_label();
						char *tmp=concat("goto ",$$.b_false);
						$$.code=concat(tmp,"\n");
			};


%%
/**
*Función principal donde vamos llamando a las funciones principales del compilador.
*/
int main(int argc, char **argv){
	FILE *i;
	if(argc!=3){
		printf("La sintaxis para ejecutar el proyecto es './proyecto <Archivo Entrada> <Archivo Salida>'.\n");
		return -1;
	}
	yyin=fopen(argv[1],"r");
	if(yyin==NULL){
		printf("No se pudo leer el archivo de entrada '%s', favor de verificarlo.\n",argv[1]);
		return -1;
	}
	yyout=fopen(argv[2],"w");
	if(yyout==NULL){
		printf("No se puede escribir en el archivo de salida '%s', favor de verificarlo.\n",argv[2]);
		return -1;
	}
	int result=yyparse();
	fclose(yyin);
	fclose(yyout);
	return result;
}

/**
*Función que nos permite personalizar los mensajes de error cuando el análisis 
*sintáctico manda error.
*
*@param s Es el apuntador que contiene el texto del error.
*/
void yyerror(char *s) {
	printf("Error en el análisis sintáctico/semántico, en la linea %d: \n%s\n ", yylineno , s );
	exit(0);
}

