/**
* Funciones de utilidad para el manejo de la gramática.
* Implementadas por:
* - Cárdenas Vallarta Josué Rodrigo
* - Madrigal Ramírez José Manuel
* - Pablo Martínez Yessica Janeth
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "gramatica_utils.h"
#include "../build/gramatica.tab.h"
#include "tabs.h"

/**
*Función que nos permite personalizar los mensajes de error cuando el análisis 
*sintáctico manda error.
*
*@param s Es el apuntador que contiene el texto del error.
*/
extern void yyerror(char *s);

/**
*Variable global que maneja el número de labels en el programa.
*/
unsigned int labels=0;

/**
*Variable global que maneja el número de temporales en el programa.
*/
unsigned int temporales=0;

/**
* Función que concatena dos cadenas y regresa una sola cadena con la unión.
* @param s1 Es el primer apuntador a evaluar.
* @param s2 Es eñ segundo apuntador a evaluar.
* @return El apuntador de la cadena nueva que contiene a las dos anteriores.
*/
char* concat(char *s1,char *s2){
	int s1_s=strlen(s1),s2_s=strlen(s2),size=s1_s+s2_s+1;
	char *result=malloc(sizeof(char)*size);
	int result_count=0;
	for(int n=0;n<s1_s;n++,result_count++){
		result[result_count]=s1[n];
	}
	for(int n=0;n<s2_s;n++,result_count++){
		result[result_count]=s2[n];
	}
	result[result_count]='\0';
	return result;
}

/**
*Función que copia una cadena y genera un nuevo apuntador con la copia.
*@param s Es la cadena a copiar.
*@return El nuevo apuntador con la copia de la cadena.
*/
char* scpy(char *s){
	int size=strlen(s);
	char *result=malloc(sizeof(char)*size);
	for(int n=0;n<size;n++){
		result[n]=s[n];
	}
	result[size]='\0';
	return result;
}

/**
*Función que convierte un entero en su representación de cadena.
*@param val Es el entero a transoformar.
*@return El apuntador a la cadena.
*/
char* int_to_str(int val){
	char *result=malloc(sizeof(char)*TAM_CADENA_DE_ENTEROS);
	sprintf(result, "%d", val);
	return result;
}

/**
*Función que regresa el apuntador de la cadena de un nuevo label.
*El label resultado siempre será diferente cada vez que se ejecute la función.
*@return El apuntador al nuevo label.
*/
char* new_label(){
	return concat("L",int_to_str(labels++));
}

/**
*Función que regresa el apuntador de la cadena de una nueva variable temporal.
*La variable resultado siempre será diferente cada vez que se ejecute la función.
*@return El apuntador a la nueva variable temporal.
*/
char* new_temp(){
	return concat("T",int_to_str(temporales++));
}

/**
*Función que añade un tipo a una lista de tipos.
*@param lista Es la lista a la que le añadiremos el elemento.
*@param num_elems Es el número de elementos que hay en la lista.
*@param tipo Es el tipo a insertar.
*/
void append_to_lista_args(unsigned int* lista,unsigned int num_elems,unsigned int tipo){
	unsigned int *lista2=realloc(lista,sizeof(unsigned int)*(num_elems+1));
	lista2[num_elems]=tipo;
	lista=lista2;
}

/**
*Función que añade una cadena a una lista de cadenas.
*@param lista Es la lista a la que le añadiremos el elemento.
*@param num_elems Es el número de elementos que hay en la lista.
*@param tipo Es la cadena a insertar.
*/
void append_to_lista_str(char** lista,unsigned int num_elems,char* str){
	char **lista2=realloc(lista,sizeof(char*)*(num_elems+1));
	lista2[num_elems]=str;
	lista=lista2;
}


/**
*Función que obtiene el tipo más grande (en caso que sea posible) entre dos tipos.
*@param id_type1 Es el primer tipo a evaluar.
*@param id_type2 Es el segundo tipo a evaluar.
*@return El tipo más grande entre los dos tipos.
*/
unsigned int type_max_id(unsigned int id_type1,unsigned int id_type2){
	if(id_type1==id_type2){
		return id_type1;
	}
	unsigned int type1=tt_tipo(id_type1),type2=tt_tipo(id_type2);
	int maximo=TOKEN_ERROR;
	if((type1!=TOKEN_CARACTER && type1!=TOKEN_ENTERO && 
		type1!=TOKEN_FLOTANTE && type1!=TOKEN_DOBLE_PRECISION) ||

	 (type2!=TOKEN_CARACTER && type2!=TOKEN_ENTERO && 
	 	type2!=TOKEN_FLOTANTE && type2!=TOKEN_DOBLE_PRECISION)){
		 yyerror("Se ha intentado hacer un cast inválido");
	}
	if(type1==TOKEN_CARACTER||type2==TOKEN_CARACTER){
		maximo=TOKEN_CARACTER;
	}
	if(type1==TOKEN_ENTERO||type2==TOKEN_ENTERO){
		maximo=TOKEN_ENTERO;
	}
	if(type1==TOKEN_FLOTANTE||type2==TOKEN_FLOTANTE){
		maximo=TOKEN_FLOTANTE;
	}
	if(type1==TOKEN_DOBLE_PRECISION||type2==TOKEN_DOBLE_PRECISION){
		maximo=TOKEN_DOBLE_PRECISION;
	}
	return tt_id_tipo(maximo);

}