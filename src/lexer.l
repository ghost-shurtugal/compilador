	/**
	* Analizador léxico implementado por:
	* - Cárdenas Vallarta Josué Rodrigo
	* - Madrigal Ramírez José Manuel
	* - Pablo Martínez Yessica Janeth
	*/

%option yylineno
%x comentario
%{
	#include "../src/gramatica_utils.h"
	#include "../src/tabs.h"
	#include <stdbool.h>  
	#include "gramatica.tab.h"
%}


letra [a-zA-Z]
tipo_reg estrr
numero [0-9]
numero_flotante {numero}{1,5}\.{numero}{1,5}
numero_doble_pres {numero}{1,10}\.{numero}{1,10}
espacio [\n\t ]
operador_relacional (>=|<=|!=|==|<|>)
simbolo_especial (\[|\]|\(|\)|\{|\}|;|,|:|\.)


%%
"¡*"	BEGIN(comentario);
<comentario>[^\*\n]*	{/*IGNORAMOS*/}
<comentario>"*"+[^\*¿]*	{/*IGNORAMOS*/}
<comentario>\n	{/*IGNORAMOS*/}
<comentario>"*¿"	{BEGIN(INITIAL);}
<comentario><<EOF>>	{
						printf("<Comentario mal formado,%s>\n",yytext);
						exit(-1);
					}
"ent"	{
			
			yylval.id_tipo=tt_id_tipo(TOKEN_ENTERO);
			return TOKEN_TIPO;
		}
"flotan"	{
			yylval.id_tipo=tt_id_tipo(TOKEN_FLOTANTE);
			return TOKEN_TIPO;
		}
"doble"	{
			yylval.id_tipo=tt_id_tipo(TOKEN_DOBLE_PRECISION);
			return TOKEN_TIPO;
		}
"carac"	{
			yylval.id_tipo=tt_id_tipo(TOKEN_CARACTER);
			return TOKEN_TIPO;
		}
"vacio"	{
			yylval.id_tipo=tt_id_tipo(TOKEN_VACIO);
			return TOKEN_TIPO;
		}
"funcion"	{return TOKEN_PALABRA_RESERVADA_FUNCION;}
"si"	{return TOKEN_PALABRA_RESERVADA_SI;}
"sino"	{return TOKEN_PALABRA_RESERVADA_SINO;}
"mientras"	{return TOKEN_PALABRA_RESERVADA_MIENTRAS;}
"para"	{return TOKEN_PALABRA_RESERVADA_PARA;}
"haz"	{return TOKEN_PALABRA_RESERVADA_HAZ;}
"regresa"	{return TOKEN_PALABRA_RESERVADA_REGRESA;}
"casos"	{return TOKEN_PALABRA_RESERVADA_CASOS;}
"detente"	{return TOKEN_PALABRA_RESERVADA_DETENTE;}
"imprime"	{return TOKEN_PALABRA_RESERVADA_IMPRIME;}
"caso"	{return TOKEN_PALABRA_RESERVADA_CASO;}
"predeterminado"	{return TOKEN_PALABRA_PREDETERMINADO;}
"cierto"	{
				//yylval.valor.valor_booleano=true;
				//yylval.valor.tipo=TOKEN_PALABRA_RESERVADA_CIERTO;
				return TOKEN_PALABRA_RESERVADA_CIERTO;
			}
"falso"	{
			//yylval.valor.tipo=TOKEN_PALABRA_RESERVADA_FALSO;
			//yylval.valor.valor_booleano=false;
			return TOKEN_PALABRA_RESERVADA_FALSO;
		}
"yy"	{return TOKEN_OPERADOR_LOGICO_AND;}
"oo"	{return TOKEN_OPERADOR_LOGICO_OR;}
"noo"	{return TOKEN_OPERADOR_LOGICO_NOT;}
'({letra}|,|\.|\ |{numero})'	{
				yylval.valor.id_tipo=tt_id_tipo(TOKEN_CARACTER);
				yylval.valor.code="";
				yylval.valor.direccion=scpy(yytext);
				return TOKEN_CARACTER;
}
\"({letra}|,|\.|\ |{numero})*\"	{
						yylval.valor.id_tipo=tt_id_tipo(TOKEN_CADENA);
						yylval.valor.code="";
						yylval.valor.direccion=scpy(yytext);
						return TOKEN_CADENA;
}
{tipo_reg}	{return TOKEN_TIPO_REGISTRO;}
{espacio}+	{/*Ignorar espacios*/}
\+	{return TOKEN_OPERADOR_MATEMATICO_MAS;}
\-	{return TOKEN_OPERADOR_MATEMATICO_MENOS;}
\*	{return TOKEN_OPERADOR_MATEMATICO_POR;}
\/ {return TOKEN_OPERADOR_MATEMATICO_ENTRE;}
\%	{return TOKEN_OPERADOR_MATEMATICO_MODULO;}
=	{return TOKEN_OPERADOR_ASIGNACION;}
{operador_relacional}	{
						yylval.str=scpy(yytext);
						return TOKEN_OPERADOR_RELACIONAL;
}
{simbolo_especial}	{ return yytext[0];}
({letra}|_)(_|{letra}|{numero})*	{
			yylval.valor.code="";
			yylval.valor.direccion=scpy(yytext);
			return TOKEN_ID;
}
{numero}{1,9}	{
		yylval.valor.id_tipo=tt_id_tipo(TOKEN_ENTERO);
		yylval.valor.code="";
		yylval.valor.direccion=scpy(yytext);
		return TOKEN_ENTERO;
}
{numero_flotante}	{
		yylval.valor.id_tipo=tt_id_tipo(TOKEN_FLOTANTE);
		yylval.valor.code="";
		yylval.valor.direccion=scpy(yytext);
		return TOKEN_FLOTANTE;
}
{numero_doble_pres}	{
		yylval.valor.id_tipo=tt_id_tipo(TOKEN_DOBLE_PRECISION);
		yylval.valor.code="";
		yylval.valor.direccion=scpy(yytext);
		return TOKEN_DOBLE_PRECISION;
}
.	{printf("Token inválido <%s> en línea %d\n",yytext,yylineno);return TOKEN_ERROR;}


%%