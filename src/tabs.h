/**
*Encabezados de las funciones de tabs.c y definición de estructuras.
*Todas estas sirven para el manejo de las tablas de símbolos y de tipos.
* Implementadas por:
* - Cárdenas Vallarta Josué Rodrigo
* - Madrigal Ramírez José Manuel
* - Pablo Martínez Yessica Janeth
*/

#include <stdbool.h>

#define SIZE_OF_ENT 4
#define SIZE_OF_FLOTAN 8
#define SIZE_OF_DOBLE 8
#define SIZE_OF_CARAC 4


/**
*	Tabla de tipos:
*/

/**
*Estructura para un registro de tipo.
*/
typedef struct _type_reg{
	unsigned int id;
	unsigned int tipo;
	unsigned int *tipo_base;
	unsigned int tamanio;
	unsigned int num_elementos;
} type_reg;

/**
*Estructura para una tabla de tipos.
*/
typedef struct _type_table{
	type_reg *registros;
	unsigned int num_regs;
} type_table;

/**
*Estructura para definir un nodo de tipo.
*/
typedef struct _node_type node_type;

struct _node_type{
	type_table *info;
	node_type *next;
};

/**
*Estructura para definir un stack de tablas de tipos.
*/
typedef struct _stack_type{
	node_type *head;
} type_table_stack;



void type_table_stack_push(type_table_stack *stack, type_table *tabla);
type_table* type_table_stack_top(type_table_stack *stack);
type_table* type_table_stack_pop(type_table_stack *stack);

unsigned int tt_id_tipo(unsigned int tipo);
unsigned int tt_tipo(unsigned int id_tipo);
unsigned int tt_tamanio(unsigned int id_tipo);

/**
*	Tabla de símbolos:
*/

#define TIPO_VALOR_VAR 1
#define TIPO_VALOR_PARAM 2 
#define TIPO_VALOR_FUNCIOM 3

/**
*Estructura para un registro de símbolos.
*/
typedef struct _sym_reg{
	unsigned int posicion;
	char* id;
	int direccion;
	unsigned int id_tipo;
	unsigned int* id_tipo_base;
	unsigned int* args;
	unsigned int n_args;
} sym_reg;

/**
*Estructura para una tabla de símboloso.
*/
typedef struct _sym_table{
	sym_reg *registros;
	unsigned int num_regs;
} sym_table;


/**
*Estructura para un nodo de símbolos.
*/
typedef struct _node_sym node_sym;

struct _node_sym{
	sym_table *info;
	node_sym *next;
};

typedef struct _stack_sym{
	node_sym *head;
} sym_table_stack;



void sym_table_stack_push(sym_table_stack *stack, sym_table *tabla);
sym_table* sym_table_stack_top(sym_table_stack *stack);
sym_table* sym_table_stack_pop(sym_table_stack *stack);



bool ts_existe(char *id);
unsigned int ts_id_tipo(char *id);
unsigned int ts_n_args(char *id);
unsigned int* ts_args(char *id);
unsigned int* ts_id_tipo_base(char *id);
void ts_insertar(char* id,unsigned int id_tipo,
			unsigned int *id_tipo_base,unsigned int* args,unsigned int n_args);


/**
* Otras operaciones.
*/
unsigned int genera_direccion(unsigned int id_tipo);
void setup_tabs();