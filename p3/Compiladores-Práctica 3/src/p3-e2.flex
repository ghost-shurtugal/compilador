%{
	#include <stdio.h>
	
	int num_palabras=0,
		num_caracteres=0,
		num_lineas=0;
%}

letra [a-zA-Z]
%%

\n {num_lineas++;num_caracteres++;}
{letra}*	{num_palabras++;num_caracteres+=yyleng;}
.	{num_caracteres++;}


%%

int main(int argc, char **argv){
	FILE *i;
	if(argc!=2){
		printf("El programa funciona sólo con un archivo.\n");
		return -1;
	}
	i = fopen(argv[1],"r");
	yyin=i;
	yylex();
	fclose(yyin);
	printf("%d %d %d %s\n",num_lineas,num_palabras,num_caracteres,argv[1]);
}
