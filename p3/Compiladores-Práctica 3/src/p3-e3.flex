%{
	#include <stdio.h>
	#include <stdlib.h> 
	
	int valor_linea=0;
%}


numero [0-9]
espacio [ ]
%%
({numero}+{espacio}*)*B	{printf("%s",yytext);}
{espacio}*	{/*Los ignoramos porque son espacios de lineas A*/}
{numero}+ {valor_linea+=atoi(yytext);}
A	{printf("%d",valor_linea);valor_linea=0;}
.	{printf("%s",yytext);}


%%

int main(int argc, char **argv){
	FILE *i;
	if(argc!=2){
		printf("El programa funciona sólo con un archivo.\n");
		return -1;
	}
	i = fopen(argv[1],"r");
	yyin=i;
	yylex();
	fclose(yyin);
}
