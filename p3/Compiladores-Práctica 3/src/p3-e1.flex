%{
	#include <stdio.h>
	#include <ctype.h>
	
%}

letra_minuscula [a-z]
espacio [ \n\t]
%%

{letra_minuscula}	{fprintf(yyout,"%c", toupper(yytext[0]));}
{espacio}*	{fprintf(yyout,"%s",yytext);}
.	{/*Ignoramos lo demás*/}


%%

int main(int argc, char **argv){
	FILE *i,*o;
	if(argc!=2){
		printf("El programa funciona sólo con un archivo.\n");
		return -1;
	}
	i = fopen(argv[1],"r");
	yyin=i;
	o = fopen("salida-e1.txt","w");
	yyout=o;
	yylex();
	fclose(yyin);
	fclose(yyout);
}
