%{
	#include <stdio.h>
	
%}

espacio [ \t\n]
%%

{espacio}*	{/*Ignoramos lo demás*/}
.	{fprintf(yyout,"%s",yytext);}


%%

int main(int argc, char **argv){
	FILE *i,*o;
	if(argc!=2){
		printf("El programa funciona sólo con un archivo.\n");
		return -1;
	}
	i = fopen(argv[1],"r");
	yyin=i;
	o = fopen("salida-e4.txt","w");
	yyout=o;
	yylex();
	fclose(yyin);
	fclose(yyout);
}
