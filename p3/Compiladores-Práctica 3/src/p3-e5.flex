%option yylineno

%{
	#include <stdio.h>
%}
numero [0-9]
caracter [a-zA-Z]
numero_real {numero}+\.{numero}+
espacio [\n\t ]
blanco [ ]
palabra_reservada (if|then|else|while|do|case|is|void|true|false|begin|end|not)
operador_aritmetico (\+|\-|\*|\/|%)
operador_relacional (!\?|\?|!|=)
simbolo_especial (\(|\)|\{|\}|;|,)
operador_asignacion :=


%%

\<\*(\n|.)*\*\>	{printf("<Comentario Multilinea,%s>\n",yytext);}
--.*\n	{printf("<Comentario Unilinea,%s>\n",yytext);}
\"({caracter}|{blanco})*\"	{printf("<Cadena de Caracteres,%s>\n",yytext);}
{espacio}+	{printf("<Espacio en blanco,%s>\n",yytext);}
{palabra_reservada}	{printf("<Palabra Reservada,%s>\n",yytext);}
{operador_aritmetico}	{printf("<Operador Aritmético,%s>\n",yytext);}
{operador_relacional}	{printf("<Operador Relacional,%s>\n",yytext);}
{operador_asignacion}	{printf("<Operador Asignación,%s>\n",yytext);}
{simbolo_especial}	{printf("<Simbolo Especial,%s>\n",yytext);}
({caracter}|_)(_|{caracter}|{numero})+	{printf("<Identificador,%s>\n",yytext);}
{numero}+	{printf("<Numero Entero,%s>\n",yytext);}
{numero_real}	{printf("<Numero Real,%s>\n",yytext);}
.	{printf("Token inválido <%s> en línea %d\n",yytext,yylineno);}


%%

int main(int argc, char **argv){
	FILE *i;
	if(argc!=2){
		printf("El programa funciona sólo con un archivo.\n");
		return -1;
	}
	i = fopen(argv[1],"r");
	yyin=i;
	yylex();
	fclose(yyin);
}
