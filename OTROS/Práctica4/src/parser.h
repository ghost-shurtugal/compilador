/*
 * Cabecera implementada por Josué Rodrigo Cárdenas Vallarta
 */
/*
 * Definimos los tipos de los términos.
 */ 
#define TERM 1
#define NO_TERM 2
#define EPSILON 3

/*
 * Especificamos la cantidad de terminales, no terminales y producciones.
 */
#define NUM_NON_TERM 7
#define NUM_TERM 10
#define NUM_PROD 14

/*
 * Especificamos a cada símbolo terminal la columna que le corresponde en
 * la MATRIZ
 */
#define COL_a 0
#define COL_b 1
#define COL_MAS 2
#define COL_POR 3
#define COL_INTERROGA 4
#define COL_PAR_ABRE 5
#define COL_v 6
#define COL_PAR_CIERRA 7
#define COL_EOF 8

/*
 * Especificamos a cada símbolo  no terminal el renglón que le 
 * corresponde en la MATRIZ
 */
#define RENG_A 0
#define RENG_AP 1
#define RENG_B 2
#define RENG_BP 3
#define RENG_C 4
#define RENG_CP 5
#define RENG_D 6

/*
 * Especificamos en cada producción a qué índice pertenece en el arreglo
 * de producciones de la estructura GRAM
 */
#define POS_PROD_A 0
#define POS_PROD_AP_vA 1
#define POS_PROD_AP_EPSILON 2
#define POS_PROD_B 3
#define POS_PROD_BP_B 4
#define POS_PROD_BP_EPSILON 5
#define POS_PROD_C 6
#define POS_PROD_CP_POR 7
#define POS_PROD_CP_MAS 8
#define POS_PROD_CP_INTERROGA 9
#define POS_PROD_CP_EPSILON 10
#define POS_PROD_D_PARENT 11
#define POS_PROD_D_a 12
#define POS_PROD_D_b 13

/*
 * Especificamos en cada símbolo a qué índice pertence en el arreglo de 
 * síbolos de la estructura GRAM
 */
#define POS_SYM_A 0
#define POS_SYM_AP 1
#define POS_SYM_B 2
#define POS_SYM_BP 3
#define POS_SYM_C 4
#define POS_SYM_CP 5
#define POS_SYM_D 6
#define POS_SYM_a 7
#define POS_SYM_b 8
#define POS_SYM_MAS 9
#define POS_SYM_POR 10
#define POS_SYM_INTERROGA 11
#define POS_SYM_PAR_ABRE 12
#define POS_SYM_v 13
#define POS_SYM_PAR_CIERRA 14
#define POS_SYM_EOF 15
#define POS_SYM_EPSILON 16

/*
 * A continuación, se listan las implementaciones de las estructuras
 */
typedef struct _symbol{
	short type;
	char *name;
	short num_col_reng;
} sym;

typedef struct _production{
	short head;
	short *body;
	short num_syms;
} prod;

typedef struct _grammar{
	short start;
	sym *syms;
	prod *prods;
	int num_prods;
	int num_syms;
} grammar;

typedef struct _node node;

struct _node{
	short info;
	node *next;
};

typedef struct _stack{
	node *head;
} stack;

grammar GRAM;

stack PILA;

int MATRIZ[NUM_NON_TERM][NUM_TERM];

void error();
void load_grammar();
void push(short sym_index);
short pop();
