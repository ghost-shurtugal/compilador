#include <stdio.h>
#include <stdlib.h>
#include "parser.h"

void error(){
	printf("Se ha encontrado un error en el texto.\nCadena NO aceptada.\n");
	exit(-1);
}

void load_sym(sym *target,short type,char *name, short num_col_reng){
	target->type=type;
	target->name=name;
	target->num_col_reng=num_col_reng;
}

void load_prod(prod *target,short head,short *body, short num_syms){
	target->head=head;
	target->body=body;
	target->num_syms=num_syms;
}

void push(short sym_index){
	node *tmp=malloc(sizeof(node));
	tmp->next=PILA.head;
	tmp->info=sym_index;
	PILA.head=tmp;
}

short pop(){
	if(!PILA.head){
		printf("Se intentó hacer un pop en la pila pero no hay elementos\nLa cadena NO es aceptada.\n");
		exit(-1);
	}
	short result=PILA.head->info;
	PILA.head=PILA.head->next;
	return result;
}

/*
 * En ésta función cargamos los datos de la gramática.
 */
void load_grammar(){
	GRAM.num_prods=7;
	GRAM.num_syms=NUM_NON_TERM+NUM_TERM;
	GRAM.syms=malloc(sizeof(sym)*GRAM.num_syms);
	load_sym(&GRAM.syms[POS_SYM_A],NO_TERM,"A",RENG_A);
	load_sym(&GRAM.syms[POS_SYM_AP],NO_TERM,"A'",RENG_AP);
	load_sym(&GRAM.syms[POS_SYM_B],NO_TERM,"B",RENG_B);
	load_sym(&GRAM.syms[POS_SYM_BP],NO_TERM,"B'",RENG_BP);
	load_sym(&GRAM.syms[POS_SYM_C],NO_TERM,"C",RENG_C);
	load_sym(&GRAM.syms[POS_SYM_CP],NO_TERM,"CP",RENG_CP);
	load_sym(&GRAM.syms[POS_SYM_D],NO_TERM,"D",RENG_D);
	load_sym(&GRAM.syms[POS_SYM_a],TERM,"a",COL_a);
	load_sym(&GRAM.syms[POS_SYM_b],TERM,"b",COL_b);
	load_sym(&GRAM.syms[POS_SYM_MAS],TERM,"+",COL_MAS);
	load_sym(&GRAM.syms[POS_SYM_POR],TERM,"*",COL_POR);
	load_sym(&GRAM.syms[POS_SYM_INTERROGA],TERM,"?",COL_INTERROGA);
	load_sym(&GRAM.syms[POS_SYM_PAR_ABRE],TERM,"(",COL_PAR_ABRE);
	load_sym(&GRAM.syms[POS_SYM_v],TERM,"v",COL_v);
	load_sym(&GRAM.syms[POS_SYM_PAR_CIERRA],TERM,")",COL_PAR_CIERRA);
	load_sym(&GRAM.syms[POS_SYM_EOF],TERM,"$",COL_EOF);
	load_sym(&GRAM.syms[POS_SYM_EPSILON],EPSILON,"",-1);
	GRAM.start=POS_SYM_A;
	GRAM.prods=malloc(sizeof(prod)*NUM_PROD);
	short *tmp;
	tmp=malloc(sizeof(short)*2);
	tmp[0]=POS_SYM_B;
	tmp[1]=POS_SYM_AP;
	load_prod(&GRAM.prods[POS_PROD_A],POS_SYM_A,tmp,2);
	
	tmp=malloc(sizeof(short)*2);
	tmp[0]=POS_SYM_v;
	tmp[1]=POS_SYM_A;
	load_prod(&GRAM.prods[POS_PROD_AP_vA],POS_SYM_AP,tmp,2);
	
	tmp=malloc(sizeof(short)*1);
	tmp[0]=POS_SYM_EPSILON;
	load_prod(&GRAM.prods[POS_PROD_AP_EPSILON],POS_SYM_AP,tmp,1);
	
	
	tmp=malloc(sizeof(short)*2);
	tmp[0]=POS_SYM_C;
	tmp[1]=POS_SYM_BP;
	load_prod(&GRAM.prods[POS_PROD_B],POS_SYM_B,tmp,2);
	
	tmp=malloc(sizeof(short)*1);
	tmp[0]=POS_SYM_B;
	load_prod(&GRAM.prods[POS_PROD_BP_B],POS_SYM_BP,tmp,1);
	
	tmp=malloc(sizeof(short)*1);
	tmp[0]=POS_SYM_EPSILON;
	load_prod(&GRAM.prods[POS_PROD_BP_EPSILON],POS_SYM_BP,tmp,1);
	
	
	tmp=malloc(sizeof(short)*2);
	tmp[0]=POS_SYM_D;
	tmp[1]=POS_SYM_CP;
	load_prod(&GRAM.prods[POS_PROD_C],POS_SYM_C,tmp,2);
	
	tmp=malloc(sizeof(short)*2);
	tmp[0]=POS_SYM_POR;
	tmp[1]=POS_SYM_CP;
	load_prod(&GRAM.prods[POS_PROD_CP_POR],POS_SYM_CP,tmp,2);
	
	tmp=malloc(sizeof(short)*2);
	tmp[0]=POS_SYM_MAS;
	tmp[1]=POS_SYM_CP;
	load_prod(&GRAM.prods[POS_PROD_CP_MAS],POS_SYM_CP,tmp,2);
	
	tmp=malloc(sizeof(short)*2);
	tmp[0]=POS_SYM_INTERROGA;
	tmp[1]=POS_SYM_CP;
	load_prod(&GRAM.prods[POS_PROD_CP_INTERROGA],POS_SYM_CP,tmp,2);
	
	tmp=malloc(sizeof(short)*1);
	tmp[0]=POS_SYM_EPSILON;
	load_prod(&GRAM.prods[POS_PROD_CP_EPSILON],POS_SYM_CP,tmp,1);
	
	
	tmp=malloc(sizeof(short)*3);
	tmp[0]=POS_SYM_PAR_ABRE;
	tmp[1]=POS_SYM_A;
	tmp[2]=POS_SYM_PAR_CIERRA;
	load_prod(&GRAM.prods[POS_PROD_D_PARENT],POS_SYM_D,tmp,3);
	
	tmp=malloc(sizeof(short)*1);
	tmp[0]=POS_SYM_a;
	load_prod(&GRAM.prods[POS_PROD_D_a],POS_SYM_D,tmp,1);
	
	tmp=malloc(sizeof(short)*1);
	tmp[0]=POS_SYM_b;
	load_prod(&GRAM.prods[POS_PROD_D_b],POS_SYM_D,tmp,1);
	
	push(POS_SYM_EOF);
	push(POS_SYM_A);
}

/*
 * Se implementa la tabla expuesta en el punto 5 del documento README.pdf
 */

int MATRIZ[NUM_NON_TERM][NUM_TERM]={
	{POS_PROD_A,POS_PROD_A,-1,-1,-1,POS_PROD_A,-1,-1,-1},
	{-1,-1,-1,-1,-1,-1,POS_PROD_AP_vA,POS_PROD_AP_EPSILON,POS_PROD_AP_EPSILON},
	{POS_PROD_B,POS_PROD_B,-1,-1,-1,POS_PROD_B,-1,-1,-1},
	{POS_PROD_BP_B,POS_PROD_BP_B,-1,-1,-1,POS_PROD_BP_B,POS_PROD_BP_EPSILON,POS_PROD_BP_EPSILON,POS_PROD_BP_EPSILON},
	{POS_PROD_C,POS_PROD_C,-1,-1,-1,POS_PROD_C,-1,-1,-1},
	{POS_PROD_CP_EPSILON,POS_PROD_CP_EPSILON,POS_PROD_CP_MAS,POS_PROD_CP_POR,POS_PROD_CP_INTERROGA,POS_PROD_CP_EPSILON,POS_PROD_CP_EPSILON,POS_PROD_CP_EPSILON,POS_PROD_CP_EPSILON},
	{POS_PROD_D_a,POS_PROD_D_b,-1,-1,-1,POS_PROD_D_PARENT,-1,-1,-1}};
