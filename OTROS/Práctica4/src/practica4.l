%{
/*
 * Práctica 4 implementada por Josué Rodrigo Cárdenas Vallarta
 */
	#include <stdio.h>
	#include <unistd.h>
	#include "parser.h"
	
	
%}

%option yylineno
%option noyywrap


%%

"a" {return POS_SYM_a;}
"b" {return POS_SYM_b;}
"+" {return POS_SYM_MAS;}
"*" {return POS_SYM_POR;}
"?" {return POS_SYM_INTERROGA;}
"(" {return POS_SYM_PAR_ABRE;}
"v" {return POS_SYM_v;}
")" {return POS_SYM_PAR_CIERRA;}
<<EOF>> {return POS_SYM_EOF;}
[ \t\n]+ {}
. {printf("Error léxico en linea %d\nCadena NO aceptada\n",yylineno);exit(-1);}//Se eligió que, al momento de encontrar un error léxico se terminara el análisis.

%%

void analisis(){
	short token=yylex();
	node *X=PILA.head;
	while(X->info!=POS_SYM_EOF){
		if(X->info==token){
			pop();
			token=yylex();
		}else{
			short tmp=MATRIZ[GRAM.syms[X->info].num_col_reng][GRAM.syms[token].num_col_reng];
			if(tmp!=-1){
				pop();
				short *body=GRAM.prods[tmp].body;
				for(int n=GRAM.prods[tmp].num_syms-1;n>-1;n--){
					if(GRAM.syms[body[n]].type!=EPSILON){
						push(body[n]);
					}
				}
			}else{
				error();
			}
		}
		X=PILA.head;
	}
	if(token==POS_SYM_EOF){
		printf("La cadena es aceptada.\n");
	}else{
		printf("La cadena NO es aceptada.\n");
	}
}


int main(int argc, char **argv){
	FILE *i;
	if(argc!=2){
		printf("El programa funciona sólo con un archivo.\n");
		return -1;
	}
	if(access( argv[1], F_OK ) == -1 ) {
		printf("El archivo no existe.\n");
		return -1;
	}
	i = fopen(argv[1],"r");
	yyin=i;
	load_grammar();
	analisis();
	fclose(yyin);
}
