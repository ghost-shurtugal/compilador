#include<stdio.h>

#ifdef PI
#define area(r) (PI*r*r)
#else
#define area(r) (3.1416*r*r)
#endif

/**
 * Compiladores 2019-2
 *
 */
int main(void){
  printf("Hola Mundo!\n");
  float mi_area=area(3);
  printf("Resultado: %f\n",mi_area);
  return 0;
}
