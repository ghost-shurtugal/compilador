%{
	#include <stdio.h>
	#include "parser.h"
	
%}

%option yylineno
%option noyywrap

id [a-zA-Z_][0-9a-zA-Z_]*
%%

{id} {return ID;}
"*" {return AST;}
"+" {return MAS;}
"(" {return PARI;}
")" {return PARD;}
[ \t\n]+ {}
. {printf("error léxico en linea %d",yylineno);}

%%

void error(){
	printf("Se ha encontrado un error en el texto.");
	exit(-1);
}

void init(){
	token=yylex();
	E();
	if(token==0){
		printf("El texto es válido.");
	}else{
		error();
	}
}


int main(int argc, char **argv){
	FILE *i;
	if(argc!=2){
		printf("El programa funciona sólo con un archivo.\n");
		return -1;
	}
	i = fopen(argv[1],"r");
	yyin=i;
	init();
	fclose(yyin);
}
