#include <stdio.h>

/*
 * Ejercicio 13
 * 
 * Definimos los arreglos que se requieren para el algoritmo de 
 * tabla compacta 
 */

int VALORES[]=      {4,1,9,2,3,5,6,7,8,9},
	COLUMNA[]=	    {1,2,7,4,6,3,0,5,6,7},
	PRIMERO_FILA[]= {0,3,4,-1,5,6,7,8,-1,9},
	NUM_FILA[]=	    {3,1,1,0,1,1,1,1,0,1};

int obten_transicion(int estado,int col_elemento){
	if(NUM_FILA[estado]==0){
		return -1;
	}
	int primero=PRIMERO_FILA[estado],
	k=0;
	while(k<NUM_FILA[estado]){
		if(COLUMNA[primero+k]==col_elemento){
			return VALORES[primero+k];
		}
		k++;
	}
	return -1;
}

int obten_columna(char elemento){
	switch(elemento){
		case 'o':
			return 0;
		case 'f':
			return 1;
		case 'i':
			return 2;
		case 'l':
			return 3;
		case 'n':
			return 4;
		case 'a':
			return 5;
		case 't':
			return 6;
		case ' ':
			return 7;
	}
	printf("El caracter '%c' no está definido.\n",elemento);
	return -1;
}


int main(int argc, char **argv){
	FILE *i;
	char actual;
	int estado=0,estado_tmp, columna_actual=-1;
	short final=0;
	if(argc!=2){
		printf("El programa funciona sólo con un archivo.\n");
		return -1;
	}
	i = fopen(argv[1],"r");
	do 
    {
      actual = (char)fgetc(i);
      if(actual==EOF){
		  break;
	  }
      columna_actual=obten_columna(actual);
      if(columna_actual!=-1){
		  estado_tmp=obten_transicion(estado,columna_actual);
		  if(estado_tmp!=-1){
			  estado=estado_tmp;
			  switch(estado){
				case 3:
					printf("Token: int\n");
					estado=0;
					break;
				case 8:
					printf("Token: float\n");
					estado=0;
					break;
				case 9:
					printf("Token de espacio.\n");
					estado=0;
					break;
			  }
		  }else{
			  printf("No se encuentra definida la transición en q%d con el caracter '%c'\n.",estado,actual);
		  }
	  }else{
		  estado=0;
	  }
    } while(1);
	fclose(i);
}
